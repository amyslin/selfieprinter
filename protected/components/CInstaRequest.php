<?php
class CInstaRequest extends CUrlHelper 
{
	const REQUEST_URI = 'https://api.instagram.com/v1/';
	const CLIENT_ID = 'a481575979d04522bd66a29460d65490';
	const CLIENT_SECRET = 'fa80a54e2ef9464cafbc3800f38df4a5';
	
	
	
	protected function getOptions() {
		return array(
				CURLOPT_HTTPHEADER=>array(
						"Content-Type: application/json; charset=UTF-8",
				)
		);
	} 
	
	
	
	public function request($endpoint = 'tags', $method = '', $params = array()) {
// 		$params['client_id'] = self::CLIENT_ID;
// 		$params['scope']="basic+public_content";
// 		$params['client_secret'] = self::CLIENT_SECRET;
		$sig = $this->getSig("/$endpoint/$method", $params);
		ksort($params);
		$params['sig'] = $sig;
		
		$query = http_build_query($params);
		$this->execute(self::REQUEST_URI . $endpoint .'/'. $method . '?' . $query, 
				$this->getOptions()
		); 
		
	}
	
	protected function getSig($endpoint, $params) {
		$sig = $endpoint;
		ksort($params);
		foreach ($params as $key=>$val) {
			$sig .= "|$key=$val";
		}
		return hash_hmac('sha256', $sig, self::CLIENT_SECRET, false);
	}
}