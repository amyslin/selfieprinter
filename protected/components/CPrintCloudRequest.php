<?php
class CPrintCloudRequest extends CUrlHelper {
	const PRINTERS_SEARCH = 'https://www.google.com/cloudprint/search';
	const PRINT_SUBMIT = 'https://www.google.com/cloudprint/submit';
	const REFRESH_TOKEN = '';
	const JOBS_URL = 'https://www.google.com/cloudprint/jobs';
	const PRINTER_URL = 'https://www.google.com/cloudprint/printer';
	const UPDATE_URL = 'https://www.google.com/cloudprint/update';

	private $access_token;

	protected function getOptions() {
		return array(
				CURLOPT_HTTPHEADER=>array(
						"Authorization: Bearer ".$this->access_token,
						"X-CloudPrint-Proxy"
				),
				CURLOPT_HTTPAUTH=>CURLAUTH_ANY
		);
	}

	public function setAccessToken($token) {
		$this->access_token = $token;
	}

	public function submitJob($printerid, $file, $contenttype = 'image/png', $tiket=array()) {
		if (!count($tiket))
			$tiket = array(
				'version'=>'1.0',
				'print'=>array(
					'vendor_ticket_item'=>array(),
					'color'=>array('type'=>'STANDARD_MONOCHROME'),
					'copies'=>array('copies'=>1),
				)
			);
// 		if ($contenttype != 'url')
// 		$tmpFile = $tmpfname = tempnam(Yii::app()->runtimePath, "png");
// 		$imageObject = imagecreatefromjpeg($file);
// 		imagepng($imageObject, $tmpFile);
		$content = base64_encode(file_get_contents($file));
// 		unlink($tmpFile);
// 		else
// 			$content = $file;
		return $this->execute(self::PRINT_SUBMIT, $this->getOptions(), array(
			'printerid'=>$printerid,
			'title'=>'Задание на печать',
			'use_cdd'=>true,
			'contentTransferEncoding' => ($contenttype=='url')?'':'base64',
			'content'=>$content,
			'contentType'=>$contenttype,
			'ticket'=> "" . json_encode($tiket) . "",
		));

	}

	public function jobs() {

	}

	public function printer($printerid) {
		return $this->execute(self::PRINTER_URL, $this->getOptions(), array(
				'printerid'=>$printerid,
				'use_cdd'=>'true',

		));
	}

	public function update($printerid, $capabilities=array(), $defaults=array()) {
		return $this->execute(self::UPDATE_URL, $this->getOptions(), array(
				'printerid'=>$printerid,
				'capabilities'=>CJSON::encode($capabilities),
				'use_cdd'=>true,
				'semantic_state'=>CJSON::encode(array(
						'version'=>'1.0',
						'printer'=>array(
							'state'=>'IDLE'
						)
				)),
// 				'defaults'=>CJSON::encode($capabilities),
		));
	}




}