<?php

class AuthUserIdentity extends CUserIdentity
{
	const ERROR_BRUTEFORCE = 11;
	
	private $_id;
	
	public function authenticate()
	{
// 		if(SrvBlockedIp::isBlocked())
// 			$this->errorCode=self::ERROR_BRUTEFORCE;
// 		else
		{
			$user=PtUser::model()->findByAttributes(array('user_name'=>$this->username,'active'=>1));
			if(empty($user))			
				$this->errorCode=self::ERROR_USERNAME_INVALID;
			else if(!$user->validatePassword($this->password))
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			else
			{
				$this->_id=$user->user_id;
				$this->username=$user->user_name;
				$this->errorCode=self::ERROR_NONE;			
			}
		}
		if($this->errorCode==self::ERROR_NONE)
			return true;
		else{
// 			SrvBlockedIp::isBlocked();
			return false;
		}
	}
	
	public function getId()
	{
		return $this->_id;
	}
	

}