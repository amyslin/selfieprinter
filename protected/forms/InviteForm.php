<?php
class InviteForm extends CFormModel {
	public $password;
	public $passwordConfirm;

	function rules()
	{
		return array(
				array('password, passwordConfirm', 'required'),
				array('password', 'match', 'pattern'=>'/[a-zA-Z0-9\&\#\$\@_]{6,}/', 'message'=>'Слишком простой пароль'),
				array('passwordConfirm', 'compare', 'compareAttribute'=>'password'),
		);
	}

	function attributeLabels()
	{
		return array(
				'password' => 'Пароль',
				'passwordConfirm' => 'Повторите пароль',
		);
	}
}