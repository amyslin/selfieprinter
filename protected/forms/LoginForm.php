<?php

class LoginForm extends CFormModel
{
	
	/**
	 * 
	 * @var string
	 */
	public $username;
	
	/**
	 * 
	 * @var string
	 */
	public $password;

	/**
	 *
	 * @var boolean
	 */
	public $remember;
	
	
	
	private $_identity;
	
	
	public function rules()
	{
		return array(
				// username and password are required
				array('username, password', 'required'),
				array('username', 'email'),
				array('remember', 'boolean'),
				// password needs to be authenticated
				array('password', 'authenticate', 'message'=>'Не верный пароль или имя пользователя'),
		);
	}
	
	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		$this->_identity=new AuthUserIdentity($this->username, $this->password);
		if(!$this->_identity->authenticate())
			$this->addError('password',$params['message']);
	}
	
	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if($this->_identity===null)
		{
			$this->_identity=new AuthUserIdentity($this->username,$this->password);
			$this->_identity->authenticate();
		}
		if($this->_identity->errorCode===AuthUserIdentity::ERROR_NONE)
		{
			$duration=3600; // 1 hour
			if ($this->remember == true) 
				Yii::app()->user->allowAutoLogin = true;
			Yii::app()->user->login($this->_identity,$duration);
			
			return true;
		}
		else
			return false;
	}
	
	
	public function attributeLabels()
	{
		return array(
			'username'=>'Email',
			'password'=>'пароль',
		);
	}
	
}