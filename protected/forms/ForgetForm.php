<?php
class ForgetForm extends CFormModel
{
	public $email;
	public $verifyCode;
	
	function rules()
	{
		return array(
			array('email', 'required'),
			array('email', 'email'),
			array('verifyCode', 'captcha', 'allowEmpty' => !Yii::app()->user->isGuest || !CCaptcha::checkRequirements(),),
		);
	}
	
	function attributeLabels()
	{
		return array(
			'э-почта' => 'э-почта',
			'verifyCode' => 'Код проверки',
		);
	}
}