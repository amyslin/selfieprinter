<?php
class PrinterStorage extends CFormModel {
	public $limit_ink;
	public $limit_paper;
	public $googleUid;
	public $serverUrl;
	// Дополнительные параметры
	
	
	public function rules() {
		return array(
			array('limit_ink,limit_paper', 'required'),
			array('limit_ink,limit_paper', 'numerical', 'integerOnly'=>true),
			array('googleUid,serverUrl', 'safe'),
		);
	}
	public function attributeLabels() {
		return array(
				'limit_ink'=>'Лимит картриджа',
				'limit_paper'=>'Лимит бумаги',
				'googleUid'=>'Идентификатор принтера в CloudPrint',
				'serverUrl'=>'Удаленный адрес принтера'
		);
	}
	
	
}