<?php
class JobStorage extends CFormModel {
	public $cnt_per_file;
	public $cnt_per_user;
	public $page_orientation;
	public $color;
	public $media_size;
	public $fit_to_page;


	const NA_10X15 = 'NA_10X15';
	const ISO_A4 = 'ISO_A4';
	const ROTATE_180 = 'ROTATE_180';
	const FLIP_ON_LONG_EDGE = 'FLIP_ON_LONG_EDGE';
	const FLIP_ON_SHORT_EDGE = 'FLIP_ON_SHORT_EDGE';
	const STANDARD_COLOR = 'STANDARD_COLOR';
	const STANDARD_MONOCHROME = 'STANDARD_MONOCHROME';
	const CUSTOM_COLOR = 'CUSTOM_COLOR';
	const CUSTOM_MONOCHROME = 'CUSTOM_MONOCHROME';
	const AUTO = 'AUTO';

	private  $MEDIA_SIZE = array(
		'NA_10X15',
		'ISO_A4'
	);

	private $PAGE_ORIENTATION = array(
		'ROTATE_180',
		'FLIP_ON_LONG_EDGE',
		'FLIP_ON_SHORT_EDGE'
	);
	private $COLOR = array(
		'STANDARD_COLOR',
		'STANDARD_MONOCHROME',
		'CUSTOM_COLOR',
		'CUSTOM_MONOCHROME',
		'AUTO'
	);

	public function rules() {
		return array(
			array('cnt_per_file,cnt_per_user,page_orientation,media_size', 'required'),
			array('cnt_per_file,cnt_per_user', 'numerical', 'allowEmpty'=>false, 'integerOnly'=>true, 'min'=>0),
			array('media_size', 'in', 'range'=>$this->MEDIA_SIZE),
			array('color', 'in', 'range'=>$this->COLOR),
			array('page_orientation', 'in', 'range'=>$this->PAGE_ORIENTATION),
		);
	}

	public function attributeLabels() {
		return array(
				'cnt_per_file'=>'Количество копий',
				'cnt_per_user'=>'Лимит на пользователя',
				'page_orientation'=>'Ориентация страницы',
				'color'=>'Цвет',
				'media_size'=>'Формат листа',
				'fit_to_page'=>'Отступы',
		);
	}
}