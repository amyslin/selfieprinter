<?php

class UserStorageForm extends CFormModel
{
	public $last_name;
	public $first_name;
    public $patronymic;


	public function rules()
	{
		return array(
			array('first_name,last_name,patronymic','safe')
		);
	}

	public function attributeLabels() {
		return array(
                    'first_name'=>'Имя',
                    'last_name'=>'Фамилия',
                    'patronymic'=>'Отчество'
		);
	}

}