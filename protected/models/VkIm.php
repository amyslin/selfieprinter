<?php
class VkIm extends EActiveRecord {
	public $onDuplicate = self::DUPLICATE_UPDATE;
	
	/**
	 *
	 * @return VkIm
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function tableName()
	{
		return 'vk_im';
	}
}