<?php
class PtRequest extends CTimeAR {
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function relations() {
		return array(
				'job' => array(self::BELONGS_TO, 'PtJob', 'job_id'),
				'item'=>array(self::HAS_ONE, 'PtJobItem', 'request_id'),
		);
	}

	public function tableName()
	{
		return 'pt_request';
	}

	public static function start() {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.state IS NULL';
		$criteria->order = 't.c_time';
		$criteria->limit = 1;
		$criteria->with = array('job', 'job.user', 'job.printer', 'item');
		$requests = PtRequest::model()->findAll($criteria);

		foreach ($requests as $request) {
			$result = self::submitRequest($request);
		}
	}

	public static function state() {

	}

	public static function submitRequest($request) {
// 		return null;
		$user = $request->job->user;
		$profile = PtProfile::model()->with(array('token'))->findByAttributes(array('user_id'=>$user->user_id, 'service'=>'google'));
		$token = $profile->token;
		$item = $request->item;
		$printer = $request->job->printer;
		$printerSettings = $printer->settings_storage;
		$printerid = $printerSettings['googleUid'];
		if ($profile->token) {

			$helper = new CPrintCloudRequest();
			$helper->setAccessToken($profile->token->access_token);
// 			print_r($request->item->source_file);
			if ($helper->submitJob($printerid, $request->item->print_file,'application/pdf',array(
				'version'=>'1.0',
				'print'=>array(
// 					'vendor_ticket_item'=>array(array('id'=>'PageMargins', 'value'=>'BORDERLESS')),
// 					'color'=>array('type'=>JobStorage::STANDARD_COLOR),
					'copies'=>array('copies'=>1),
					'page_orientation'=>array('type'=> 'AUTO'),
					'media_size'=>array("width_microns"=>101600, "height_microns"=>152400, "vendor_id"=>"w288h432", "is_continuous_feed"=>true),
					'margins'=>array('top_microns'=>0,'right_microns'=>0,'bottom_microns'=>0,'left_microns'=>0),
					'fit_to_page'=>array('type'=>'FIT_TO_PAGE'),

				)
			))) {

				$response = CJSON::decode($helper->content, false);
				echo $helper->content;
				if(!empty($response->success)&&$response->success) {
					if(!empty($response->job->status)) {
// 						print_r($response->job->status);
						$request->state = $response->job->status;
						$request->source_uid = $response->job->id;
						$request->save(false);
// 						return $item->save(false);
					}
				}




			}
			else {
				echo $helper->content;
			}

		}
	}
}