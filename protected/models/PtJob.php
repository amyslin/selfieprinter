<?php
class PtJob extends CTimeAR {
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName() {
		return "pt_job";
	}

	public function relations() {
		return array(
				'printer' => array(self::BELONGS_TO, 'PtPrinter', 'printer_id'),
				'user' => array(self::BELONGS_TO, 'PtUser', 'user_id'),
				'template' => array(self::BELONGS_TO, 'PtTemplate', 'template_id'),
		);
	}

	public static function check() {
		self::model()->findAll(array('condition'=>'job_status = "RUN"'));
	}

	protected function beforeSave() {
		if (parent::beforeSave()) {
			if ($this->isNewRecord)
				$this->job_status = "NEW";
			$this->storage = serialize($this->storage);
			return true;
		}
		else return false;
	}

	protected function afterFind()
	{
		parent::afterFind();
		$this->storage=unserialize($this->storage);
	}
}