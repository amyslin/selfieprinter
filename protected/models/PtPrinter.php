<?php
class PtPrinter extends CTimeAR {
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function tableName() {
		return "pt_printer";
	}
	
	
	protected function beforeSave() {
		if (parent::beforeSave()) {
			$this->settings_storage = serialize($this->settings_storage);
			return true;
		}
		else return false;
	}
	
	protected function afterFind()
	{
		parent::afterFind();
		$this->settings_storage=unserialize($this->settings_storage);
	}
}