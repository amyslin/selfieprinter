<?php
class PtUser extends CTimeAR {


	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('user_name', 'required'),
				array('user_name', 'length', 'max'=>128),
				array('user_name', 'email', 'message'=>'Невалидный email'),
// 				array('profile_id', 'safe'),
		);
	}

	public function tableName() {
		return "pt_user";
	}

	public function relations() {
		return array(
				'profiles' => array(self::HAS_MANY, 'PtProfile', 'user_id'),
		);
	}


	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			$this->storage=serialize($this->storage);
			if($this->isNewRecord){
				$this->user_salt=new CDbExpression('sha1(uuid())');
				$this->recovery_uid = base_convert(sha1(uniqid(rand(), true)),16, 36);
				$this->recovery_expire = new CDbExpression("ADDDATE(NOW(), INTERVAL +3 hour)");
			}
			return true;
		}
		else
			return false;

	}

	/**
	 *
	 * @param string $password
	 * @return boolean
	 */
	public function validatePassword($password)
	{
		return CPasswordHelper::same(sha1($this->user_salt.$password), $this->user_password);
	}

	protected function afterFind()
	{
		parent::afterFind();
		$this->storage=unserialize($this->storage);
	}





}