<?php

class AuthAssignment extends CTimeAR
{

	/**
	 *
	 * @return AuthAssignment
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'auth_assignment';
	}
}
