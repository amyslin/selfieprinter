<?php
class PtJobItem extends EActiveRecord {
	public $onDuplicate = self::DUPLICATE_UPDATE;
	
	const STATE_NEW = "NEW";
	const STATE_PRINTING = "PRINTING";
	const STATE_DONE = "DONE";
	const STATE_ERROR = "ERROR";
	
	/**
	 *
	 * @return IdAddress
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	public function tableName()
	{
		return 'pt_job_item';
	}
	
	public function relations() {
		return array(
			'job' => array(self::BELONGS_TO, 'PtJob', 'job_id'),
		);
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->c_time = new CDbExpression('now()');
			}
			return true;
		}
		else
			return false;
	}
}