<?php
class OAuthToken extends CTimeAR {

	/**
	 * Returns the static model of the specified AR class.
	 * @return PtProfile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'oauth_access';
	}
}