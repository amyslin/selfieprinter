<?php
class PtProfile extends CTimeAR {
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return PtProfile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function relations() {
		return array(
				'user' => array(self::BELONGS_TO, 'PtUser', 'user_id'),
				'token'=>array(self::HAS_ONE, 'OAuthToken', 'profile_id', 'condition'=>'token.expired <> 1'),
		);
	}
	
	public function tableName()
	{
		return 'pt_profiles';
	}
}