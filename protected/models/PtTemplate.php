<?php
class PtTemplate extends CTimeAR {
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function relations() {
		return array(

		);
	}



	public function tableName()
	{
		return 'pt_template';
	}

	public function renderTemplate($params) {
		$svg = simplexml_load_string($this->data);
		foreach($svg->getDocNamespaces() as $strPrefix => $strNamespace) {
			if(strlen($strPrefix)==0) {
				$strPrefix="a"; //Assign an arbitrary namespace prefix.
			}
			$svg->registerXPathNamespace($strPrefix,$strNamespace);
		}

		foreach ($params as $name=>$param) {
			$type = 'image';
			if (isset($param["type"]))
				$type = $param["type"];

			switch ($type) {
				default:
				case 'image':
					@$elements = $svg->xpath("//{$type}[@id='$name']");

					if(count($elements))
					for($i = 0; $i < count($elements); $i++) {
// 						if(!empty($elements[$i]->attributes('xlink', true)->href)&&isset($param['value']))
							$elements[$i]->attributes('xlink', true)->href = 'data:image/jpeg;base64,' . base64_encode(file_get_contents($param['value']));
// 						elseif(isset($param['value']))
// 							$elements[$i]->addAttribute('xlink:href', 'data:image/jpeg;base64,'. base64_encode(file_get_contents($param['value'])));
					}

				break;
				case 'text':
					@$elements = $svg->xpath("//{$type}[@id='$name']/tspan");
					if(count($elements))
						for($i = 0; $i < count($elements); $i++) {
							if(isset($param['value']))
							$elements[$i][0] = $param['value'];
						}
				break;
			}



		}


		$imagick = new Imagick();
		$imagick->readImageBlob($svg->asXML());
		// 		$imagick->setresolution(300, 300);
		// 		$imagick->resampleImage(150,150,imagick::FILTER_UNDEFINED,1);
		$imagick->setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
		$imagick->setimageformat('png');
		return $imagick->getImageBlob();

	}

}