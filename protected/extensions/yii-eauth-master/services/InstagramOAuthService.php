<?php
class InstagramOAuthService extends EOAuth2Service {
	protected $name = 'instagram';
	protected $title = 'Instagram';
	protected $type = 'OAuth';
	protected $jsArguments = array('popup' => array('width' => 1010, 'height' => 560));

	protected $client_id = '';
	protected $client_secret = '';
	protected $scope = '';
	protected $providerOptions = array(
			'authorize' => 'https://api.instagram.com/oauth/authorize',
			'access_token' => 'https://api.instagram.com/oauth/access_token',
	);
	
	public $token;

	protected $uid = null;

// 	protected function fetchAttributes() {
// 		$info = (array)$this->makeSignedRequest('https://api.dropbox.com/1/account/info');
// 		$this->attributes['name'] = $info['full_name'];
// 	}
	protected function getTokenUrl($code) {
		return $this->providerOptions['access_token'];
	}

	protected function getTokenOptions($code) {
		return array(
			'client_id'=>$this->client_id,
			'client_secret'=>$this->client_secret,
			'grant_type'=>'authorization_code',
			'redirect_uri'=>$this->getState('redirect_uri'),
			'code'=>$code,
		);
	}

	protected function getAccessToken($code) {
		$token = $this->makeRequest($this->getTokenUrl($code), array('data'=>$this->getTokenOptions($code)));
		$this->token = $token; 
		return $token; 
	}
}