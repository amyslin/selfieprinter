<?php
$cs=Yii::app()->clientScript;
$cs->registerCssFile('/css/pages/login.css');
$cs->registerPackage('bootstrap');
?>
<div class="form">
<div class="text-center">
<h4>Письмо отправлено!</h3>
Проверьте почту.<br>
Письмо содержит инструкцию по изменению пароля.
</div>
<?= CHtml::link('Назад', ['site/login'], ['class' => 'btn btn-default']) ?>
</div>