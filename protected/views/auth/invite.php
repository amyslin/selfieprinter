<?php
$cs=Yii::app()->clientScript;
$cs->registerCssFile('/css/pages/login.css');
$cs->registerPackage('bootstrap');
?>
<div class="form">
<!-- BEGIN LOGIN FORM -->
<?php $form = $this->beginWidget('CActiveForm',
		array(
				'id'=>'user-login',
				'enableAjaxValidation'=>true,
				'method'=>'post',
		)
);?>
		<h3 class="form-title">Ввод пароля</h3>
		<div class="form-group">
	
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<?php echo $form->label($model, 'password', array('class'=>'control-label visible-ie8 visible-ie9'));?>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
				<?php echo $form->passwordField($model, 'password', array('placeholder'=>'Пароль', 'class'=>'form-control placeholder-no-fix')); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->label($model, 'passwordConfirm', array('class'=>'control-label visible-ie8 visible-ie9'));?>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
				<?php echo $form->passwordField($model, 'passwordConfirm', array('placeholder'=>'Повторите пароль', 'autocomplete'=>'off', 'class'=>'form-control placeholder-no-fix')); ?>
			</div>
		</div>
		<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message):?>
    	<?php $class=($key=='error')?"alert alert-danger":"alert alert-success"?>
        <div id="flash-<?=$key?>" class="<?=$class?>"><?=$message?></div>
	<? endforeach; ?>
	<div class="row">
		<div class="form-group" style="text-align: center">
<!-- 			<label class="checkbox"> -->
<!-- 			<input type="checkbox" name="remember" value="1"/> Remember me </label> -->
			<?php echo CHtml::link('Назад', array('site/login'), array('class'=>'btn btn-default'))?>&nbsp;&nbsp;
			<?php echo CHtml::submitButton('Сохранить пароль', array('class'=>'btn btn-info'))?>
		
		</div>
	</div>
	<?php $this->endWidget();?>
	<!-- END LOGIN FORM -->
</div>