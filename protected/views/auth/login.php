<?php
$cs=Yii::app()->clientScript;
$cs->registerCssFile('/css/pages/login.css');
// $cs->registerPackage('jquery-migrate');
// $cs->registerPackage('bootstrap-hover-dropdown');
// $cs->registerPackage('jquery-slimscroll');
// $cs->registerPackage('jquery-blockui');
// $cs->registerPackage('jquery-cokie');
// $cs->registerPackage('uniform');
// $cs->registerPackage('jquery-validation');
// $cs->registerPackage('select2');
$cs->registerPackage('bootstrap');
// $cs->registerScriptFile('/scripts/app.js', CClientScript::POS_END);
// $cs->registerScriptFile('/scripts/login.js', CClientScript::POS_END);

//$cs->registerScript($this->getId(),'App.init(); Login.init();');

$this->pageTitle = 'Вход'; 
?>
<div class="form">
	<!-- BEGIN LOGIN FORM -->
	<?php $form = $this->beginWidget('CActiveForm', 
			array(
				'id'=>'user-login',
				'enableAjaxValidation'=>false,
				'enableClientValidation'=>false,
				'method'=>'post',
			)
	);?>
		<h3 class="form-title">Авторизация</h3>
		<div class="alert alert-error display-hide">
			<button class="close" data-close="alert"></button>
			<span>
				 Авторизация
			</span>
		</div>
		<div class="form-group">
	
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<?php echo $form->label($model, 'username', array('class'=>'control-label visible-ie8 visible-ie9'));?>
			<div class="input-icon">
				<i class="fa fa-envelope-o"></i>
				<?php /*echo $form->textField($model, 'username', array('placeholder'=>'Логин', 'autocomplete'=>'off', 'class'=>'form-control placeholder-no-fix'));*/ ?>
				<?php
					echo $form->textField($model, 'username', array('placeholder'=>'Email', 'autocomplete'=>'off', 'class'=>'form-control placeholder-no-fix'));
				?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->label($model, 'password', array('class'=>'control-label visible-ie8 visible-ie9'));?>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
				<?php echo $form->passwordField($model, 'password', array('placeholder'=>'Пароль', 'autocomplete'=>'off', 'class'=>'form-control placeholder-no-fix')); ?>
			</div>
		</div>
		<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message):?>
        <div id="flash-<?=$key?>" class="alert alert-danger"><?=$message?></div>
	<?php endforeach; ?>
		<div class="form-group">
<!-- 			<label class="checkbox"> -->
<!-- 			<input type="checkbox" name="remember" value="1"/> Remember me </label> -->
			<?php echo CHtml::button('Вход', array('class'=>'btn btn-info pull-right', 'type'=>'submit'))?>
			<div style="display: inline-block">
				<?php echo CHtml::link('Восстановить пароль', array('auth/forget'))?>
			</div>
			
		</div>
	<?php $this->endWidget();?>
	<!-- END LOGIN FORM -->
</div>
<?php 
$script = <<<SCRIPT
document.onkeyup = function (e) {
	e = e || window.event;
	if (e.keyCode === 13) {
		$('#user-login').submit();
	}
	return true;
}
SCRIPT;
$cs=Yii::app()->getClientScript();
$cs->registerScript('loginForm',$script, CClientScript::POS_READY);
?>
