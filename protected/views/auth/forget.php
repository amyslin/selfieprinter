<?php
$cs=Yii::app()->clientScript;
$cs->registerCssFile('/css/pages/login.css');
$cs->registerPackage('bootstrap');
?>
<div class="form">
<?php $form = $this->beginWidget('CActiveForm', array(
    'id'=>'forget-form',
    'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
	'htmlOptions'=>array(
		'class' => "forget-form",
		'method' => 'post'
	)
)); ?>

<?php if (!empty($model->errors)): ?>
	<div class="alert alert-danger">
		<span>
			<?php echo $form->errorSummary($model); ?>
		</span>
	</div>	
<?php endif; ?>

<form class="forget-form" method="post">
	<p>
		Email
	</p>
	<div class="alert alert-error display-hide">
		<button class="close" data-close="alert"></button>
		<span>
			 Введите телефон и пароль.
		</span>
	</div>
	<div class="form-group">
		<div class="input-icon">
			<i class="fa fa-envelope-o"></i>
			<?php
					echo $form->textField($model, 'email', array('placeholder'=>'Email', 'autocomplete'=>'off', 'class'=>'form-control placeholder-no-fix'));
				?>
		</div>
	</div>
	<div class="form-group">
			<div class="col-md-6" style="padding-left: 0;">
			<?php if (CCaptcha::checkRequirements() && Yii::app()->user->isGuest) {
				$this->widget('CCaptcha', [
					'buttonLabel' => '<span class="fa fa-refresh"></span> обновить картинку',
					'buttonOptions' => ['style' => 'display: block;']
				]);
			} ?>
		</div>
		<div class="col-md-6" style="padding-right: 0;">
			<?= CHtml::activeTextField($model, 'verifyCode', [
				'class' => "form-control placeholder-no-fix", 
				'autocomplete' => "off",
				'placeholder' => "код с картинки",
			]) ?>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="form-group">
		<?= CHtml::link('Назад', array('auth/login'), array('class' => 'btn btn-default')) ?>
		<button type="submit" class="btn btn-info pull-right">Отправить</button>
	</div>
<?php $this->endWidget(); ?>
</div>