<?php $this->beginContent('/layouts/main'); ?>
<body class="login">
	<div class="content">
	<?php echo $content; ?>
	<div class="copyright">
		 2015 &copy; selfieprinter.ru
	</div>
	</div>
<!-- BEGIN COPYRIGHT -->

<!-- END COPYRIGHT -->
<!--[if lt IE 9]>
<script src="/scripts/respond.min.js"></script>
<script src="/scripts/excanvas.min.js"></script> 
<![endif]-->
</body>
<?php $this->endContent(); ?>