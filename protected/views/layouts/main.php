<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title><?php echo Yii::app()->name . ($this->pageTitle ? ' - ' . $this->pageTitle : ''); ?></title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <meta name="MobileOptimized" content="320">
   <!-- BEGIN GLOBAL MANDATORY STYLES --> 
<?php  
	$cs=Yii::app()->clientScript;
	$cs->registerPackage('font-awesome');
	$cs->registerPackage('bootstrap');
	$cs->registerPackage('uniform');
?>         
   <!-- END GLOBAL MANDATORY STYLES -->
   <!-- BEGIN THEME STYLES --> 
   <link href="/css/style-conquer.css" rel="stylesheet" type="text/css"/>
   <link href="/css/style.css" rel="stylesheet" type="text/css"/>
   <link href="/css/style-responsive.css" rel="stylesheet" type="text/css"/>
   <link href="/css/plugins.css" rel="stylesheet" type="text/css"/>
   <link href="/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
   <link href="/css/custom.css" rel="stylesheet" type="text/css"/>
   <!-- END THEME STYLES -->
   <link rel="shortcut icon" href="/favicon.ico" />
</head>
<!-- END HEAD -->

<?php echo $content;?>

</html>