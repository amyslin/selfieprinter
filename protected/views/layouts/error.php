<?php
$cs=Yii::app()->clientScript;
// $cs->registerPackage('font-awesome');
$cs->registerPackage('bootstrap');
// $cs->registerPackage('funiform');
$cs->registerCssFile('/css/style-conquer.css');
$cs->registerCssFile('/css/style.css');
$cs->registerCssFile('/css/plugins.css');
$cs->registerCssFile('/css/themes/default.css');
$cs->registerCssFile('/css/pages/error.css');
$cs->registerCssFile('/css/custom.css');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="ru" class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<title>Селфипринтер | Ошибка приложения</title>
<meta name="MobileOptimized" content="320">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<link rel="shortcut icon" href="/favicon.ico"/>
</head>
<body class="page-500-full-page">
<?=$content?>
	<h3 class="page-500"><a href="/">На главную</a> | <a href="/login">Авторизация</a></h3>
</body>
