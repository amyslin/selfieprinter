<?php

date_default_timezone_set('Europe/Moscow');
mb_internal_encoding("UTF-8");
mb_regex_encoding("UTF-8");
// Yii::setPathOfAlias('ext', dirname(__FILE__).'/../extensions');
$request = new CHttpRequest();
$common = array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'selfieprinter',
    'language' => 'ru',
    'defaultController' => 'site',
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.forms.*',
    	'application.services.*',
// 		'ext.PHPMailer.*'
    ),
    // application components
    'components' => array(
        'user' => array(
            // это значение устанавливается по умолчанию
            'loginUrl' => array('auth/login'),
			'allowAutoLogin'=>true,
        ),
        'request' => array(
            'enableCsrfValidation' => false,
        ),
    	'errorHandler'=>array(
        	'errorAction'=>'site/error',
        ),
    	'eauth' => array(
    				'class' => 'ext.yii-eauth-master.EAuth',
    				'popup' => true, // Use the popup window instead of redirecting.
    				'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache'.
    				'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
    				'services' => array( // You can change the providers and their classes.
    						'instagram' => array(
    								// register your app here: https://vk.com/editapp?act=create&site=1
    								'class' => 'InstagramOAuthService',
    								'client_id' => 'a481575979d04522bd66a29460d65490',
    								'client_secret' => 'fa80a54e2ef9464cafbc3800f38df4a5',
    								'cancelUrl' => '/',
    								'scope' => 'basic public_content relationships likes',
    						),
    						'vk' => array(
    								// register your app here: https://vk.com/editapp?act=create&site=1
    								'class' => 'VKontakteOAuthService',
    								'client_id' => '5510075',
    								'client_secret' => 'Wu82oUvWOPavuvKRvgz8',
    								'cancelUrl' => '/',
    								'scope' => 'groups+messages+offline',
    						),
    						'google' => array(
    								// register your app here: https://vk.com/editapp?act=create&site=1
    								'class' => 'GoogleOAuthService',
    								'client_id' => '213827193990-bv0tlgrgp516bgihg2dhrns5k0vlnqt2.apps.googleusercontent.com',
    								'client_secret' => 'i6mwn_5A_kDvzOZS6NKr3zht',
    								'cancelUrl' => '/',
    								'scope' => 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/cloudprint',
    						),
    				),
    	),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'caseSensitive' => false,
            'rules' => array(
            		'/privacy/<lng:\w+>'=>'/site/privacy',
         			'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                	'cabinet/<controller:\w+>/<action:\w+>' => 'cabinet/<controller>/<action>',
            		
// 		'crm/<controller:\w+>/<action:\w+>' => 'crm/<controller>/<action>',
//                 '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
// 		'/jobs' => '/site/jobs',
// 		'/purchase' => '/site/purchase',
// 		'/selection' => '/site/selection',
// 		'/partnership' => '/site/partnership',
// 		'/deal' => '/site/deal'
            ),
        ),
		'authManager'=>array(
			'class'=>'CDbAuthManager',
			'itemTable'=>'auth_item',
			'itemChildTable'=>'auth_item_child',
			'assignmentTable'=>'auth_assignment',
		),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning, application',
                    'logFile' => 'application.log',
                    'maxFileSize' => 4096,
                    'maxLogFiles' => 2,
                ),
            	array(
            			'class' => 'CWebLogRoute',
            			'categories' => 'application',
            			'levels'=>'error, warning, trace, profile, info',
            	),
                /*array(
                    'class' => 'CFileLogRoute',
                    'levels' => '',
                    'logFile' => 'trace.log',
                    'maxFileSize' => 8192,
                    'maxLogFiles' => 5,
                ),*/
            ),
        ),
        'session' => array(
            'class' => 'CHttpSession',
            'timeout' => 1800,
        ),

        'clientScript' => (preg_match('/cabinet|auth/', $request->pathInfo))? require dirname(__FILE__) . '/clientscript.php' : require dirname(__FILE__) . '/clientscript_site.php',
    ),
	'modules'=>array(
			'cabinet'=>array(),
	),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
//     'params' => require(dirname(__FILE__) . '/params.php'),
);

return CMap::mergeArray($common, require(dirname(__FILE__) . '/db.php'));
