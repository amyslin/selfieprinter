<?php
$browser = get_browser();
$includeIE = ($browser->browser='IE' && $browser->majorver>8 && $browser->majorver<10);

return array(
		'coreScriptPosition'=>CClientScript::POS_END,
		'packages'=>array(
			'jquery'=>array(
						'basePath'=>'application.plugins',
						'js'=>array('jquery-2.1.4.min.js'),
			),
				
			'owl-carousel'=>array(
					'basePath'=>'application.plugins.owl-carousel',
					'js'=>array('owl.carousel.min.js'),
					'css'=>array('owl.carousel.css', 'owl.theme.css', 'owl.transitions.css'),
					'depends'=>array('jquery'),
			),
			'bootstrap'=>array(
						'basePath'=>'application.plugins.bootstrap',
						'js'=>array('js/bootstrap.min.js'),
						'css'=>array('css/bootstrap.min.css'),
						'depends'=>array('jquery'),
			),
			'revolution-slider'=>array(
						'basePath'=>'application.plugins.revolution-slider',
						'js'=>array('js/jquery.themepunch.tools.min.js', 'js/jquery.themepunch.revolution.min.js'),
						'css'=>array('css/settings.css'),
						'depends'=>array('jquery'),
			),
			'nav'=>array(
						'basePath'=>'application.plugins',
						'js'=>array('jquery.nav.min.js'),
						'depends'=>array('jquery'),
			),
			'easing'=>array(
						'basePath'=>'application.plugins',
						'js'=>array('jquery.easing.1.3.js'),
						'depends'=>array('jquery'),
			),
			'modernizr'=>array(
						'basePath'=>'application.plugins',
						'js'=>array('modernizr.min.js'),
						'depends'=>array('jquery'),
			),
		),
);