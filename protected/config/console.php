<?php

$db = require(dirname(__FILE__).'/db.php');

$config=CMap::mergeArray($db,array(
		
	'name'=>'selfieprinter console',
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'selfieprinter.ru',
	'language' => 'ru',
	'preload' => array( 'log' ),
	// autoloading model and component classes
	'import'=>array(
			'application.models.*',
			'application.components.*',
			'application.forms.*',
			'application.services.*'
	),
	'components'=>array(
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
					'logFile'=>'console.log',
					'maxFileSize'=>4096,
					'maxLogFiles'=>2,
				),
			),
		),
	),
));

return $config;