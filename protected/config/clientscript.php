<?php
return array(
		'coreScriptPosition'=>CClientScript::POS_END,
		'packages' => array(
// 				'jquery'=>array(
// 						'basePath'=>'application.assets.plugins.scripts',
// 						'js'=>array('jquery-1.11.0.min.js'),
// 				),
// 				'jquery.ui'=>array(
// 						'basePath'=>'application.assets.plugins.jquery-ui',
// 						'js'=>array('jquery-ui-1.10.3.custom.min.js'),
// 						'css'=>array('jquery-ui-1.10.3.custom.min.css '),
// 						'depends'=>array('jquery'),
// 				),
				'font-awesome'=>array(
						'basePath'=>'application.assets.plugins.font-awesome',
						'css'=>array('css/font-awesome.min.css'),
						'depends'=>array('jquery'),
				),
				'bootstrap'=>array(
						'basePath'=>'application.assets.plugins.bootstrap',
						'js'=>array('js/bootstrap.min.js'),
						'css'=>array('css/bootstrap.min.css'),
						'depends'=>array('jquery'),
				),
				'simple-line-icons'=>array(
						'basePath'=>'application.assets.plugins.simple-line-icons',
						'js'=>array('icons-lte-ie7.js'),
						'css'=>array('simple-line-icons.min.css'),
						// 						'depends'=>array('jquery'),
				),
// 				'blockui' => array(
// 						'basePath'=>'application.assets.plugins.jquery-blockui',
// 						'js'=>array('jquery.blockui.min.js'),
// 						'depends'=>array('jquery'),
// 				),
				'uniform'=>array(
						'basePath'=>'application.assets.plugins.uniform',
						'js'=>array('jquery.uniform.min.js'),
						'css'=>array('css/uniform.default.css'),
						'depends'=>array('jquery'),
				),
				'select2'=>array(
						'basePath'=>'application.assets.plugins.select2',
						'css'=>array(
								'select2.css',
								'select2-bootstrap.css'
						),
						'js'=>array('select2.min.js'),
						'depends'=>array('bootstrap'),
				),
				'jquery-migrate'=>array(
						'basePath'=>'application.assets.plugins.scripts',
						'js'=>array('jquery-migrate-1.2.1.min.js'),
						'depends'=>array('jquery'),
				),
				'bootstrap-toastr'=>array(
						'basePath'=>'application.assets.plugins.bootstrap-toastr',
						'css'=>array('toastr.min.css'),
						'js'=>array('toastr.min.js'),
						'depends'=>array('bootstrap'),
				),
				'bootstrap-switch'=>array(
						'basePath'=>'application.assets.plugins.bootstrap-switch',
						'css'=>array('css/bootstrap-switch.min.css'),
						'js'=>array('js/bootstrap-switch.min.js'),
						'depends'=>array('bootstrap'),
				),
				'bootstrap-hover-dropdown'=>array(
						'basePath'=>'application.assets.plugins.bootstrap-hover-dropdown',
						'js'=>array('twitter-bootstrap-hover-dropdown.min.js'),
				),
				'jquery-slimscroll'=>array(
						'basePath'=>'application.assets.plugins.jquery-slimscroll',
						'js'=>array('jquery.slimscroll.min.js'),
						'depends'=>array('jquery'),
				),
				'jquery-blockui'=>array(
						'basePath'=>'application.assets.plugins.scripts',
						'js'=>array('jquery.blockui.min.js'),
						'depends'=>array('jquery'),
				),
				'jquery-cokie'=>array(
						'basePath'=>'application.assets.plugins.scripts',
						'js'=>array('jquery.cokie.min.js'),
						'depends'=>array('jquery'),
				),
				'jquery-uniform'=>array(
						'basePath'=>'application.assets.plugins.uniform',
						'js'=>array('jquery.uniform.min.js'),
						'depends'=>array('jquery'),
				),
				'jquery-validation'=>array(
						'basePath'=>'application.assets.plugins.jquery-validation',
						'js'=>array('dist/jquery.validate.min.js'),
						'depends'=>array('jquery'),
				),
				'jquery-form'=>array(
						'basePath'=>'application.assets.plugins.jquery-form',
						'js'=>array('jquery.form.min.js', 'jquery.MultiFile.js'),
						'depends'=>array('jquery'),
				),
				'simplemodal'=>array(
						'basePath'=>'application.assets.plugins.simplemodal',
						'js'=>array('jquery.simplemodal.1.4.4.min.js'),
						'depends'=>array('jquery'),
				),
				'bootstrap-timepicker'=>array(
						'basePath'=>'application.assets.plugins.bootstrap-timepicker',
						'js'=>array('js/bootstrap-timepicker.js'),
						'css'=>array('compiled/timepicker.css'),
						'depends'=>array('bootstrap'),
				),
				'bootstrap-datepicker'=>array(
						'basePath'=>'application.assets.plugins.bootstrap-datepicker',
						'js'=>array('js/bootstrap-datepicker.js'),
						'css'=>array('css/datepicker.css'),
						'depends'=>array('bootstrap'),
				),
				'bootstrap-daterangepicker'=>array(
						'basePath'=>'application.assets.plugins.bootstrap-daterangepicker',
						'js'=>array('moment.min.js','daterangepicker.js'),
						'css'=>array('daterangepicker-bs3.css'),
						'depends'=>array('bootstrap-datepicker'),
				),
				'bootstrap-wysihtml5'=>array(
						'basePath'=>'application.assets.plugins.bootstrap-wysihtml5',
						'js'=>array('wysihtml5-0.3.0.js', 'bootstrap-wysihtml5.js'),
						'css'=>array('bootstrap-wysihtml5.css', 'wysiwyg-color.css'),
						'depends'=>array('bootstrap'),
				),
				'flot'=>array(
						'basePath'=>'application.assets.plugins.flot',
						'js'=>array('jquery.flot.min.js'),
						'depends'=>array('jquery'),
				),
				'flot-time'=>array(
						'basePath'=>'application.assets.plugins.flot',
						'js'=>array('jquery.flot.time.min.js'),
						'depends'=>array('jquery'),
				),
				'flot-stack'=>array(
						'basePath'=>'application.assets.plugins.flot',
						'js'=>array('jquery.flot.stack.min.js'),
						'depends'=>array('jquery'),
				),
				'ckeditor'=>array(
						'basePath'=>'application.assets.plugins.ckeditor',
						'js'=>array('ckeditor.js'),
				),
				'codemirror'=>array(
					'basePath'=>'application.assets.plugins.codemirror',
					'js'=>array(
						'lib/codemirror.js',
						'addon/edit/matchbrackets.js',
						'mode/htmlmixed/htmlmixed.js',
						'mode/xml/xml.js',
						'mode/javascript/javascript.js',
						'mode/css/css.js',
						'mode/clike/clike.js',
						'mode/php/php.js',
						'addon/display/fullscreen.js',
					),
					'css'=>array(
						'lib/codemirror.css',
						'addon/display/fullscreen.css',
					),
				),
		),
);