<?php
class SiteController extends CController {

	public function init() {
		$this->setPageTitle('SelfiePrinter.RU - Продажа, аренда, установка и обслуживание Инстапринтеров в Перми');
	}
	
	public function actions()
	{
		return array(
				'captcha' => array(
						'class' => 'CCaptchaAction',
				),
		);
	}

	public function actionIndex() {
		
		Yii::app()->theme = 'atropos';
		$this->layout= 'main';
		
		$this->render('index');
	}
	public function actionPrivacy($lng = 'ru') {
		Yii::app()->theme = 'atropos';
		$this->layout= 'privacy';
		
		switch ($lng) {
			default:
			case 'ru':
				$this->render('privacy_ru');
			break;
			case 'en':
				$this->render('privacy_en');
			break;
		}
		
		
	}

	public function actionError()
	{

		$this->layout = 'error';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', array('error' => $error));
		}
	}
	
	


}