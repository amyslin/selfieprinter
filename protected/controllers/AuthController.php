<?php
class AuthController extends CController {
	
	
	public function init()
	{
		$this->layout = 'auth';
		$this->defaultAction = 'login';
	}
	
	public function accessRules()
	{
		return array(
				array('allow',
						'actions' => array('login', 'forget', 'captcha', 'error', 'logout', 'restore', 'invite'),
						'users' => array('*')
				),
				array('denny', 'users'=>array('*')),
		);
	}
	
	public function actions()
	{
		return array(
				'captcha' => array(
						'class' => 'CCaptchaAction',
				),
		);
	}
	
	public function actionLogin() {
		$user=Yii::app()->user;

		$model = new LoginForm;
		if (isset($_POST['LoginForm'])) {
			$model->setAttributes($_POST['LoginForm']);

			if ($model->validate()&&$model->login()) {
				$this->redirect('/cabinet');
			}
			else {
				$messages = array();
				foreach($model->errors as $error) {
					$messages[] = $error[0];
				}
				Yii::app()->user->setFlash('error', implode('<br/>', $messages));
			}
		}
		$this->render('login', array('model'=>$model));
	}
	
	public function actionForget()
	{
		$model = new ForgetForm;
		$this->layout = 'auth';
		// Если передаана почта и код с картинки, проверяем код и отправляем инструкцию
		if (isset($_POST['ForgetForm']))
		{
			$model->attributes = $_POST['ForgetForm'];
				
			if ($model->validate()) {
	
				$user = PtUser::model()->findByAttributes(array('user_name'=>$model->email));
				if (empty ($user)) {
					$model->addError('email', 'Пользователь с таким e-mail не существует');
				}
				else {
					$user->recovery_uid = base_convert(sha1(uniqid(rand(), true)),16, 36);
					$user->recovery_expire = new CDbExpression("ADDDATE(NOW(), INTERVAL +3 hour)");
					$user->save(false);
					$user->refresh();
// 					$body = <<<BODY
// <p>Мы получили запрос на смену пароля к Вашему личному кабинету сервиса оценки заемщиков Scorista.ru, находящегося по адресу <a href="https://cabinet.scorista.ru/">https://cabinet.scorista.ru/</a> </p>.
// <p>Пожалуйста, перейдите по ссылке для того, чтобы сменить Ваш пароль:</p>
// <p><a href="https://cabinet.scorista.ru/site/restore/?uid={$user->recovery_uid}">https://cabinet.scorista.ru/site/restore/?uid={$user->recovery_uid}</a></p>
// <p>Обратите внимание: эта ссылка действительна только 3 часа. Пожалуйста, выполните смену пароля в течение этого времени.</p>
// <p>Если Вы не хотите менять Ваш пароль или получили это письмо по ошибке, пожалуйста, проигнорируйте его или обратитесь в службу поддержки клиентов за разъяснениями <a href="mailto:support@scorista.ru">support@scorista.ru</a> .</p>
// <p>Если у Вас есть какие-либо вопросы, то Вы можете свободно связаться с нами по адресу <a href="mailto:support@scorista.ru">support@scorista.ru</a></p>
// <p>С наилучшими пожеланиями, <br/>
// Команда Scorista.ru<br/>
// <a href="http://www.scorista.ru">www.scorista.ru</a></p>
// BODY;
	
// 					CUtils::sendMail($model->email, 'Запрос на смену пароля к сервису Скориста.ру', $body);
					$this->redirect(array('auth/forget', 'complete' => 1));
				}
			}
		}
	
		// Если письмо с инструкцией отправлено
		if (isset($_GET['complete'])) {
			$this->render('forget_complete');
		}
		// Выводим форму восстановления
		else {
			$this->render('forget', array('model' => $model));
		}
	}
	
	public function actionRestore($uid) {
		if (empty($uid)) {
			throw new CException('Неверный код восстановления');
		}
		$user = PtUser::model()->findByAttributes(array(
				'recovery_uid'=>$uid,
		));
			
		$model = new RestoreForm();
		if (empty($user) || (strtotime($user->recovery_expire) < time())) {
			Yii::app()->user->setFlash('error', 'Неверный код восстановления');
			// 				$model->addError('password', 'Неверный код восстановления');
		}
		elseif(isset($_POST['RestoreForm'])) {
			$model->setAttributes($_POST['RestoreForm']);
			if ($model->validate()) {
				if (empty($user->user_salt))
					$user->user_salt = new CDbExpression('sha1(uuid())');
				$user->user_password = sha1($user->user_salt.$model->password);
				$user->save(false);
				Yii::app()->user->setFlash('success', 'Пароль успешно изменен');
			}
			else {
				$messages = array();
				foreach($model->errors as $error) {
					$messages[] = $error[0];
				}
				Yii::app()->user->setFlash('error', implode('<br/>', $messages));
			}
		}
		$this->render('restore', array('model'=>$model));
			
			
	}
	
	public function actionInvite($uid) {
		if (empty($uid)) {
			throw new CException('Неверный код');
		}
		$user = PtUser::model()->findByAttributes(array(
				'recovery_uid'=>$uid,
		));
			
		$model = new InviteForm();
		if (empty($user)) {
			Yii::app()->user->setFlash('error', 'Неверный код');
			// 				$model->addError('password', 'Неверный код восстановления');
		}
		elseif(isset($_POST['InviteForm'])) {
			$model->setAttributes($_POST['InviteForm']);
			if ($model->validate()) {
				if (empty($user->user_salt))
					$user->user_salt = new CDbExpression('sha1(uuid())');
					$user->active = 1;
				$user->user_password = sha1($user->user_salt.$model->password);
				$user->save(false);
				Yii::app()->user->setFlash('success', 'Пароль успешно сохранен');
			}
			else {
				$messages = array();
				foreach($model->errors as $error) {
					$messages[] = $error[0];
				}
				Yii::app()->user->setFlash('error', implode('<br/>', $messages));
			}
		}
		$this->render('invite', array('model'=>$model));
	}
	
	
}