<?php
class TestController extends CController {
	public function actionIndex() {
		$request = new CInstaRequest();
		$request->request('tags', 'nofilter/media/recent', array());
		print_r($request);
	}

	public function actionMakeInvite() {
		$user = new PtUser;
		$user->user_name = 'Myslin.pavel@gmail.com';
		$user->save(false);
	}
	
	public function actionRussia() {
		$testText = <<<TEXT
ткакрыорыммваымпвыамёаывпиапи
апи
апи
апиапиваип
иапиваымпаыиавп
иап
[НАИМЕНОВАНИЕ_ТЕГА:описание]данные[/НАИМЕНОВАНИЕ_ТЕГА]
[TAG2:описание]данные
ываыва
апвпвва
вапвапвап
вапвапвап
[/TAG2]
[TAG3:описание]данные[/TAG3]
тексап ываорыварлывраукп пваылподыволап
[TAG4:описание]данные[/TAG4]
TEXT;
		$regexp = '/\[([^\:^\[]+)\:([^\]]*)\]([^\[]*)\[\/([^\]]+)\]/m';
		preg_match_all($regexp, $testText, $matches);
		//print_r($matches);
		$result1 = array();
		$result2 = array();
		if (count($matches)==5) {
			foreach($matches[1] as $key => $val) {
				if ($val == $matches[4][$key]) {
					$result1[][$val] = $matches[3][$key];
					$result2[][$val] = $matches[2][$key];
				}
			}
		}
		print_r($result1);
		print_r($result2);
	}
	
	public function actionInterface($id) {
		
		Yii::app()->theme = 'atropos';
		$this->layout= 'interface';
		
		
		$provider = new CActiveDataProvider('PtRequest', array(
				'criteria'=>array(
					'with'=>array('item', 'job'),
					'condition'=>'t.printer_id = :printer_id AND job.job_status = "RUN" AND item.print_file IS NOT NULL AND t.source_uid IS NULL AND t.state IS NULL',
					'params'=>array(':printer_id'=>$id),
// 					'order'=>'t.c_time ASC'
				),
				'sort'=>array(
					'defaultOrder'=>'t.c_time DESC',
				),
				'pagination' =>array(
						'pageSize'=>20,
				)
		));
		$this->render('index', array('provider'=>$provider));
	}
	
	

	public function actionListprinters() {
		$user = PtUser::model()->findByPk(1);
		$profile = PtProfile::model()->with(array('token'))->findByAttributes(array('user_id'=>$user->user_id, 'service'=>'google'));
		$token = $profile->token;
// 		print_r($token);
		if(!empty($profile->token)) {
// 			echo  'request';
			$helper = new CUrlHelper();
			$helper->execute('https://www.google.com/cloudprint/search', array(
					CURLOPT_HTTPHEADER=>array(
							"Authorization: Bearer ".$profile->token->access_token,
							"Content-Type: application/json"
// 							"X-CloudPrint-Proxy"
					),
					CURLOPT_HTTPAUTH=>CURLAUTH_ANY
			));
			print_r($helper->content);
		}
	}

	public function actionPrint($id) {
		//PtRequest::start();
		$request = PtRequest::model()->with(array('job', 'job.user', 'job.printer', 'item'))->findByPk($id);
		if(!empty($request) && is_null($request->state))
			PtRequest::submitRequest($request);
		Yii::app()->end();
// 		$user = PtUser::model()->findByPk(1);
// 		$profile = PtProfile::model()->with(array('token'))->findByAttributes(array('user_id'=>$user->user_id, 'service'=>'google'));
// 		$token = $profile->token;
// 		$printer = PtPrinter::model()->findByPk(2);
// 		$settings = $printer->settings_storage;
// 		$printerid = $settings['googleUid'];
// 		print_r($printerid);
// 		// 		print_r($token);2
// 		if(!empty($profile->token)) {
// 			// 			echo  'request';
// 			$content = file_get_contents(Yii::app()->getRuntimePath() . DIRECTORY_SEPARATOR . 'print.jpg');
// 			$helper = new CPrintCloudRequest();
// 			$helper->setAccessToken($profile->token->access_token);

// 			$helper->submitJob($printerid, Yii::app()->getRuntimePath() . DIRECTORY_SEPARATOR . 'print.jpg');
// 			print_r($helper->content);
// 		}

	}

	public function actionPrinter() {
		$printer = PtPrinter::model()->findByPk(9);
		$printerSettings = $printer->settings_storage;
		$printerid = $printerSettings['googleUid'];
		$user = PtUser::model()->findByPk(1);
		$profile = PtProfile::model()->with(array('token'))->findByAttributes(array('user_id'=>$user->user_id, 'service'=>'google'));
		$helper = new CPrintCloudRequest();
		$helper->setAccessToken($profile->token->access_token);
		$helper->printer($printerid);

		print_r($helper->content);

	}
	
	public function actionWidget() {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.job_status = "RUN"';
		$criteria->order = 't.c_time';
		
		$jobs = PtJob::model()->findAll($criteria);
		foreach ($jobs as $job) {
			$itemsCount = PtJobItem::model()->count(array('condition'=>'t.job_id = :job_id', 'params'=>array(':job_id'=>$job->job_id)));
			if (($job->time_limit>0) && (strtotime($job->c_time) + ($job->time_limit*60) > time())) {
				$job->job_status = "FINISHED";
				$job->save(false);
			}
			else if ($job->cnt_limit > 0 && ($itemsCount >= $job->cnt_limit)) {
				$job->job_status = "FINISHED";
				$job->save(false);
			}
			else {
				$className= ucfirst($job->type).'Service';
				if(class_exists($className)){
					$service=new $className();
					$service->update($job);
				}
			}
		}
	}
	
	private function getMediaInfo($url) {
		$helper = new CUrlHelper();
		$helper->execute($url);
		
		
		$res = preg_replace('/(<script[^>]*>.*?<\/script>)/is', "", $helper->content);
		$res = preg_replace('/(<noscript[^>]*>.*?<\/noscript>)/is', "", $res);
		$res = preg_replace('/(<style[^>]*>.*?<\/style>)/is', "", $res);
		// 		$res = mb_convert_encoding($res, 'utf-8', mb_detect_encoding($res));
		if($helper->isHttpOK()) {
			$xmlPath=new XmlPath($res, $html=true);
			
			$result =  $xmlPath->queryAll(array(
				'source_img'=>'//div[@class="main-photo-big"][1]//img[1]/@src',
				'userpic'=>'//div[@class="profilephoto"][1]//img[1]/@src',
				'username'=>'//span[@class="blueusername"][1]/text()',
				'userlname'=>'//div[@class="username fl"][1]//span[@class="date"][1]/text()',
			), null, true);
// 			if (!count($result))
// 				return null;
			preg_match('/\/([\d_]+)\//', $url, $matches);
			if(count($matches)==2) {
				$extra = explode('_', $matches[1]);
				$result['source_id'] = $matches[1];
				$result['user_id'] = $extra[1];
			}
			return $result;
			
		}
		else 
			return null;
		
	}

	public function actionUpdatePrinter() {
		$printer = PtPrinter::model()->findByPk(9);
		$printerSettings = $printer->settings_storage;
		$printerid = $printerSettings['googleUid'];
		$user = PtUser::model()->findByPk(1);
		$profile = PtProfile::model()->with(array('token'))->findByAttributes(array('user_id'=>$user->user_id, 'service'=>'google'));
		$helper = new CPrintCloudRequest();
		$helper->setAccessToken($profile->token->access_token);
		$helper->update($printerid, array(
				'version'=>'1.0',
				'printer'=>array(
					'margins'=>array('option'=>array(array('top_microns'=>0,'right_microns'=>0,'bottom_microns'=>0,'left_microns'=>0, 'is_default'=>true, 'type'=>'BORDERLESS'))),
					'supported_content_type'=>array(array('content_type'=>'application/pdf'), array('content_type'=>'image/png'), array('content_type'=>'image/jpeg')),
				),
		));
		print_r($helper->content);

	}

	public function actionJobs() {
		$user = PtUser::model()->findByPk(1);
		$profile = PtProfile::model()->with(array('token'))->findByAttributes(array('user_id'=>$user->user_id, 'service'=>'google'));
		$token = $profile->token;
		$printer = PtPrinter::model()->findByPk(2);
		$settings = $printer->settings_storage;
		$printerid = $settings['googleUid'];
		$helper = new CPrintCloudRequest();
		$helper->setAccessToken($profile->token->access_token);
		$helper->jobs($printerid);
		print_r($helper->content);
	}
	public function actionJob() {
		$user = PtUser::model()->findByPk(1);
		$profile = PtProfile::model()->with(array('token'))->findByAttributes(array('user_id'=>$user->user_id, 'service'=>'google'));
		$token = $profile->token;
		$printer = PtPrinter::model()->findByPk(2);
		$settings = $printer->settings_storage;
		$printerid = $settings['googleUid'];
		$helper = new CPrintCloudRequest();
		$helper->setAccessToken($profile->token->access_token);
		$helper->job('22be4de3-06e5-b9c0-6f06-cd4db480a9fc');
		print_r($helper->content);
	}

	public function actionTag() {
		$user = PtUser::model()->findByPk(1);
		$profile = PtProfile::model()->with(array('token'))->findByAttributes(array('user_id'=>$user->user_id, 'service'=>'instagram'));
		$token = $profile->token;
		$helper = new CInstaRequest();
		$helper->request('tags', 'printertest2/media/recent', array(
			'access_token'=>$token->access_token,
// 			'lat'=>'58.004626',
// 			'lng'=>'56.246489',
// 			'distance'=>500
// 			'q'=>'instaprinter'
			'count'=>25,
		));
		print_r($helper->content);
	}

	public function actionExt() {
		preg_match('/\/([\d_]+)\//', 'http://www.intagme.com/andreimyslin/1130354235220312747_2281967703/', $matches);
		print_r($matches);
	}

	public function actionTemplate() {
// 		$svgSource = file_get_contents(Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR.'template.svg');
		$item = PtJobItem::model()->with(array('job'))->findByPk(9);
// 		print_r($item);
// 		$svg = new DomDocument();
		$svg = simplexml_load_file(Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR.'10x15.svg');
// 		print_r($svg->getDocNamespaces());
		foreach($svg->getDocNamespaces() as $strPrefix => $strNamespace) {
			if(strlen($strPrefix)==0) {
				$strPrefix="a"; //Assign an arbitrary namespace prefix.
			}
			$svg->registerXPathNamespace($strPrefix,$strNamespace);
		}
// 		$svg->xpath("//image[@id='userpic']");
		$data = CJSON::decode($item->source_data);
// 		print_r($data);
		$picSource = base64_encode(file_get_contents($data['user']['profile_picture']));
		@$userpic = $svg->xpath("//image[@id='userpic']");
		$userpic[0]->attributes('xlink', true)->href = 'data:image/jpeg;base64,' . $picSource;
		$imageSource = base64_encode(file_get_contents($item->source_file));
		@$img = $svg->xpath("//image[@id='image']");
		$img[0]->attributes('xlink', true)->href = 'data:image/jpeg;base64,' . $imageSource;
		@$username = $svg->xpath("//text[@id='username']/tspan");
		$username[0][0] = $data['user']['username'];
		@$tag = $svg->xpath("//text[@id='tag']/tspan");
		$tag[0][0] = '#' . $item->job->search_query;
// 		var_dump();
// 		echo $svg->asXML();
// 		Yii::app()->end();

		$imagick = new Imagick();
		$imagick->readImageBlob($svg->asXML());
// 		$imagick->setresolution(300, 300);
// 		$imagick->resampleImage(150,150,imagick::FILTER_UNDEFINED,1);
		$imagick->setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
		$imagick->setimageformat('png');
		header("Content-Type: image/png");
		echo $imagick;

	}

	private function prepareUserpic($url) {
		$image = file_get_contents($url);
		$mask = new Imagick(Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR.'mask.png');
		$im = new Imagick();
		$im->readImageBlob($image);
		$im->compositeImage($mask, Imagick::COMPOSITE_DSTIN, 0, 0, Imagick::CHANNEL_ALPHA);
		$im->setimageformat('png');
		return $im->getImageBlob();
	}
	
	public function actionLoadKrawlly() {
		$dirs = scandir(Yii::app()->runtimePath . DIRECTORY_SEPARATOR . 'krawlly');
		foreach ($dirs as $dir) {
			
			if (!in_array($dir, array(".", ".."))) {
// 				print_r($dir);
				$command = Yii::app()->db2->createCommand();
				$command->select = '*';
				$command->from = 'krawlly_item t';
				$command->where = 't.bank = :bank';
				$cnt = $command->queryScalar(array(':bank'=>$dir));
				if(!$cnt) {
					$command = Yii::app()->db2->createCommand("INSERT INTO krawlly_item (bank) VALUES (:bank)");
					$command->execute(array(':bank'=>$dir)); 
					$this->loadData($dir);
// 					$command->from = 'krawlly_item t';
// 					$command->where = 't.bank = :bank';
				}
				else {
					$this->loadData($dir);
				}
				
			}
		}
	}
	
	private function loadData($dir) {
		$command = Yii::app()->db2->createCommand();
		$command->select = '*';
		$command->from = 'krawlly_item t';
		$command->where = 't.bank = :bank';
		$command->params = array(':bank'=>$dir);
		$rows = $command->queryRow();
		if (isset($rows['id'])) {
			$this->scanFolder($dir, $rows['id']);
		}
		
		
		
// 		print_r($rows);
	}
	
	private function scanFolder($dir, $bankId) {
		$files = scandir(Yii::app()->runtimePath . DIRECTORY_SEPARATOR . 'krawlly' . DIRECTORY_SEPARATOR . $dir);
		count($files);
		foreach($files as $file) {
			if(!in_array($file, array(".", ".."))) {
				$content = file_get_contents(Yii::app()->runtimePath . DIRECTORY_SEPARATOR . 'krawlly' . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR.$file);
				$json = json_decode($content, true);
// 				print_r($json);
				$this->parseJson($json, $bankId);
				
			}
		}
		
		
		
	}
	
	private function parseJson($json, $bankId) {
		$infoId = null;
		$columns = Yii::app()->db2->createCommand("SHOW COLUMNS FROM krawlly_info")->queryColumn();
		$values = array();
		if (!empty($json['info'])||!empty($json['profile'])) {
			$data = (!empty($json['profile']))?$json['profile']:$json['info'];
			
			foreach ($columns as $column) {
				if (!empty($data[$column])) 
					$values[$column] = (preg_match('/date|time|birthday/i', $column))?date('Y-m-d h:i:s' ,(strlen($json['info'][$column])>10)?substr($json['info'][$column], 0, -3):$json['info'][$column]):$json['info'][$column];
				
			}		
			
		}
		else {
			foreach ($columns as $column) {
				if (!empty($json[$column])) {
					$values[$column] = (preg_match('/date|time|birthday/i', $column))?date('Y-m-d h:i:s' ,(strlen($json[$column])>10)?substr($json[$column], 0, -3):$json[$column]):$json[$column];
				}
			}

		}
		if(count($values)) {
			$values['item_id'] = $bankId;
			Yii::app()->db2->createCommand()->insert("krawlly_info", $values);
			$infoId = Yii::app()->db2->getLastInsertID();
		}
		
		else {
			print_r($values);
		}
		if(!empty($infoId)&&!empty($json['cards'])) {
				
			$columns = Yii::app()->db2->createCommand("SHOW COLUMNS FROM krawlly_cards")->queryColumn();
			foreach ($json['cards'] as $item) {
				$values = array();
				$cardId = null;
				foreach ($columns as $column) {
					if (!empty($item[$column]))
						$values[$column] = (preg_match('/date|time|birthday/i', $column))?date('Y-m-d h:i:s' ,(strlen($item[$column])>10)?substr($item[$column], 0, -3):$item[$column]):$item[$column];
				
				}
				if (count($values)) {
					$values['info_id']=$infoId;
					Yii::app()->db2->createCommand()->insert("krawlly_cards", $values);
					$cardId = Yii::app()->db2->getLastInsertID();	
				}
				if (!empty($item['transactions'])) {
					$cols = Yii::app()->db2->createCommand("SHOW COLUMNS FROM krawlly_transactions")->queryColumn();
					$values = array();
					foreach ($item['transactions'] as $transaction) {
						foreach ($cols as $column) {
							if (!empty($transaction[$column]))
								$values[$column] = (preg_match('/date|time|birthday/i', $column))?date('Y-m-d h:i:s' ,(strlen($transaction[$column])>10)?substr($transaction[$column], 0, -3):$transaction[$column]):$transaction[$column];
						
						}
						if (count($values)) {
							$values['card_id']=$cardId;
							Yii::app()->db2->createCommand()->insert("krawlly_transactions", $values);
// 							$cardId = Yii::app()->db2->getLastInsertID();
						}
					}
						
				}
			}
			
		}
		if(!empty($infoId)&&!empty($json['accounts'])) {
		
			$columns = Yii::app()->db2->createCommand("SHOW COLUMNS FROM krawlly_accounts")->queryColumn();
			foreach ($json['accounts'] as $item) {
				$values = array();
				$cardId = null;
				foreach ($columns as $column) {
					if (!empty($item[$column]))
						$values[$column] = (preg_match('/date|time|birthday/i', $column))?date('Y-m-d h:i:s' ,(strlen($item[$column])>10)?substr($item[$column], 0, -3):$item[$column]):$item[$column];
		
				}
				if (count($values)) {
					$values['info_id']=$infoId;
					Yii::app()->db2->createCommand()->insert("krawlly_accounts", $values);
					$cardId = Yii::app()->db2->getLastInsertID();
				}
				if (!empty($item['transactions'])) {
					$cols = Yii::app()->db2->createCommand("SHOW COLUMNS FROM krawlly_transactions")->queryColumn();
					$values = array();
					foreach ($item['transactions'] as $transaction) {
						foreach ($cols as $column) {
							if (!empty($transaction[$column]))
								$values[$column] = (preg_match('/date|time|birthday/i', $column))?date('Y-m-d h:i:s' ,(strlen($transaction[$column])>10)?substr($transaction[$column], 0, -3):$transaction[$column]):$transaction[$column];
		
						}
						if (count($values)) {
							$values['account_id']=$cardId;
							Yii::app()->db2->createCommand()->insert("krawlly_transactions", $values);
// 							$cardId = Yii::app()->db2->getLastInsertID();
						}
					}
		
				}
			}
				
		}
		
		if(!empty($infoId)&&!empty($json['deposits'])) {
		
			$columns = Yii::app()->db2->createCommand("SHOW COLUMNS FROM krawlly_deposits")->queryColumn();
			foreach ($json['deposits'] as $item) {
				$values = array();
				$cardId = null;
				foreach ($columns as $column) {
					if (!empty($item[$column]))
						$values[$column] = (preg_match('/date|time|birthday/i', $column))?date('Y-m-d h:i:s' ,(strlen($item[$column])>10)?substr($item[$column], 0, -3):$item[$column]):$item[$column];
		
				}
				if (count($values)) {
					$values['info_id']=$infoId;
					Yii::app()->db2->createCommand()->insert("krawlly_deposits", $values);
					$cardId = Yii::app()->db2->getLastInsertID();
				}
				if (!empty($item['transactions'])) {
					$cols = Yii::app()->db2->createCommand("SHOW COLUMNS FROM krawlly_transactions")->queryColumn();
					$values = array();
					foreach ($item['transactions'] as $transaction) {
						foreach ($cols as $column) {
							if (!empty($transaction[$column]))
								$values[$column] = (preg_match('/date|time|birthday/i', $column))?date('Y-m-d h:i:s' ,(strlen($transaction[$column])>10)?substr($transaction[$column], 0, -3):$transaction[$column]):$transaction[$column];
		
						}
						if (count($values)) {
							$values['deposit_id']=$cardId;
							Yii::app()->db2->createCommand()->insert("krawlly_transactions", $values);
// 							$cardId = Yii::app()->db2->getLastInsertID();
						}
					}
		
				}
			}
		
		}
		
		if(!empty($infoId)&&!empty($json['loans'])) {
		
			$columns = Yii::app()->db2->createCommand("SHOW COLUMNS FROM krawlly_loans")->queryColumn();
			foreach ($json['loans'] as $item) {
				$values = array();
				$cardId = null;
				foreach ($columns as $column) {
					if (!empty($item[$column]))
						$values[$column] = (preg_match('/date|time|birthday/i', $column))?date('Y-m-d h:i:s' ,(strlen($item[$column])>10)?substr($item[$column], 0, -3):$item[$column]):$item[$column];
		
				}
				if (count($values)) {
					$values['info_id']=$infoId;
					Yii::app()->db2->createCommand()->insert("krawlly_loans", $values);
					$cardId = Yii::app()->db2->getLastInsertID();
				}
				if (!empty($item['transactions'])) {
					$cols = Yii::app()->db2->createCommand("SHOW COLUMNS FROM krawlly_loan_transactions")->queryColumn();
					$values = array();
					foreach ($item['transactions'] as $transaction) {
						foreach ($cols as $column) {
							if (!empty($transaction[$column]))
								$values[$column] = (preg_match('/date|time|birthday/i', $column))?date('Y-m-d h:i:s' ,(strlen($transaction[$column])>10)?substr($transaction[$column], 0, -3):$transaction[$column]):$transaction[$column];
		
						}
						if (count($values)) {
							$values['loan_id']=$cardId;
							Yii::app()->db2->createCommand()->insert("krawlly_loan_transactions", $values);
// 							$cardId = Yii::app()->db2->getLastInsertID();
						}
					}
		
				}
				if (!empty($item['schedule'])) {
					$cols = Yii::app()->db2->createCommand("SHOW COLUMNS FROM krawlly_schedule")->queryColumn();
					$values = array();
					foreach ($item['schedule'] as $transaction) {
						foreach ($cols as $column) {
							if (!empty($transaction[$column]))
								$values[$column] = (preg_match('/date|time|birthday/i', $column))?date('Y-m-d h:i:s' ,(strlen($transaction[$column])>10)?substr($transaction[$column], 0, -3):$transaction[$column]):$transaction[$column];
				
						}
						if (count($values)) {
							$values['loan_id']=$cardId;
							Yii::app()->db2->createCommand()->insert("krawlly_schedule", $values);
// 							$cardId = Yii::app()->db2->getLastInsertID();
						}
					}
				
				}
			}
		
		}
		
		
		print_r($cont);
		
	}
	
	public function actionInsta() {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.job_status = "RUN"';
		$criteria->order = 't.c_time';
		
		$jobs = PtJob::model()->findAll($criteria);
		foreach ($jobs as $job) {
			$itemsCount = PtJobItem::model()->count(array('condition'=>'t.job_id = :job_id', 'params'=>array(':job_id'=>$job->job_id)));
			if (($job->time_limit>0) && (strtotime($job->c_time) + ($job->time_limit*60) > time())) {
				$job->job_status = "FINISHED";
				$job->save(false);
			}
			else if ($job->cnt_limit > 0 && ($itemsCount >= $job->cnt_limit)) {
				$job->job_status = "FINISHED";
				$job->save(false);
			}
			else {
				$className= ucfirst($job->type).'Service';
				if(class_exists($className)){
					$service=new $className();
					$service->update($job);
				}
			}
		}
		
	}
	
	public function actionSortfiles() {
		$path_from = Yii::app()->runtimePath . "/Daze16/"; 
		$types = array('jpg', 'png', 'jpeg', 'gif');
		$files = scandir($path_from);
		$filenames = array();
		$products = array();
		$sort = array();
		foreach($files as $file) {
			if ($file == '.' || $file == '..')
			{
				continue;
			}
			if (preg_match("/^([\.\-\d]+)(.*)(\d+\." .implode("|\d+\.", $types) .")$/i", $file, $matches)) {
				//print_r($matches);
				if(isset($matches[1])) {
					$products[$matches[1]][] = $file;
				}
				if (isset($matches[3]))
					$sort[$matches[1]][] = substr($matches[3], 0, 1);
				
			}
			
		}
		foreach($products as &$product) {
			shuffle($product);
		}
		array_multisort($sort, SORT_NUMERIC, $products);
		print_r($sort);
		print_r($products);
	}
}