<?php
return array(
		// application components
		'components'=>array(
				'db'=>array(
						'connectionString' => 'mysql:host=localhost;dbname=hashprint;port=3316',
						'username' => 'root',
						'password' => 'La5haBbn',
						'charset' => 'utf8',
						'enableParamLogging'=>true,
				),
				'db2'=>array(
						'class'=>'CDbConnection',
						'connectionString' => 'mysql:host=localhost;dbname=imported;port=3316',
						'username' => 'root',
						'password' => 'La5haBbn',
						'charset' => 'utf8',
						'enableParamLogging'=>true,
				),
		),
);
