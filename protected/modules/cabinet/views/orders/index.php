<div class="row">
      <div class="col-md-12">
        <?php
            echo CHtml::link('Добавить ордер', array('add'), array('class'=>'btn btn-success btn-sm pull-right'));
        ?>
      </div>
</div>
<div class="row" style="margin-top: 20px;">
<div class="col-md-12">
<div class="portlet">
<div class="portlet-title">
    <div class="caption">
        <i class="fa fa-print"></i> Ордеры на печать
    </div>
</div>
<div class="portlet-body">
<div class="dataTables_filter pull-right" style="margin-bottom: 20px;">
	<form id="frmOrgSearch" method="GET" class="form-inline" role="form">
			<div class="form-group">

			</div>
			<div class="form-group">
			<button class="btn btn-success" type="submit">
				<i class="fa fa-search"></i>
			</button>
			</div>
	</form>
</div>
<?php

$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider'=>$provider,
		'selectableRows' => 0,
		'afterAjaxUpdate'=>'tableOnUpdate',
		'htmlOptions'=>array('class'=>'table-responsive','style'=>'cursor:pointer;'),
		'itemsCssClass'=>'table table-bordered table-hover',
		'template'=>'<div class="overlay"></div><div style="padding-bottom:10px;padding-top: 10px; font-size:1.1em;">{summary}</div>{items}{pager}',
		'summaryText'=>'{start}-{end} из {count}',
		'pagerCssClass'=>'pagination',
		'pager'=>array(
			'header'=>'',
			'htmlOptions'=>array('class'=>'pagination'),
		),
		'columns'=>array(
			array('name'=>'job_id', 'header'=>'ID'),
			array('name'=>'type', 'header'=>'Тип'),
			array('name'=>'search_query', 'header'=>'Хэштэг'),
			array('name'=>'printer.name', 'header'=>'Принтер'),
			array('name'=>'c_time', 'header'=>'Дата', 'type'=>'raw', 'value'=>'date("d.m.Y H:i", strtotime($data->c_time))'),
			array('name'=>'user.user_name', 'header'=>'Пользователь'),
			array('name'=>'job_status', 'header'=>'Статус', 'type'=>'raw', 'value'=>'Yii::app()->controller->renderJobStatus($data)'),
			array(
						'class'=>'CButtonColumn',
						'template' => '{play} {pause} {stop}',
						'buttons' => array(
							'play' => array(
									'label' => '<i class="fa fa-play"></i>',
									'url' => '"?id=".$data->job_id."&status=RUN"',
									'options'=>array('class' =>'btn btn-xs btn-primary status_btn'),
							),
							'pause' => array(
										'label' => '<i class="fa fa-pause"></i>',
										'url' => '"?id=".$data->job_id."&status=PAUSED"',
										'options'=>array('class' =>'btn btn-xs btn-primary status_btn'),
							),
							'stop' => array(
										'label' => '<i class="fa fa-stop"></i>',
										'url' => '"?id=".$data->job_id."&status=FINISHED"',
										'options'=>array('class' =>'btn btn-xs btn-primary status_btn'),
							),
						)
			),
			array(
                'class'=>'CButtonColumn',
                'template' => '{update} {view}',
                'buttons' => array(
				    'update' => array('label' => 'Редактировать','url' => 'Yii::app()->controller->createUrl("orders/update/id/" . $data->job_id)'),
					'view' => array('label' => 'Редактировать','url' => 'Yii::app()->controller->createUrl("orders/items/id/" . $data->job_id)')
                    )
            )
		)
));
?>
</div>
</div>
</div>
</div>
<?php
$script = <<<SCRIPT
//$('.select2me').select2();
//$('.select2me').change(function(){ $('#frmOrgSearch').submit(); });
init();
function tableOnUpdate() {
	init();
}
function changeStatus(url) {
	$.ajax(url, {
		dataType: 'json',
		type : 'GET',
	})
    .done(function(data){
		if (data.status == 'OK')
      		$.fn.yiiGridView.update('yw0');
      	if (data.status == 'ERROR')
      		toastr.error(data.message);
	})
    .error(function(data){
		toastr.error("Ошибка отавки запроса");
	});
}
function init() {
	$('.status_btn').click(function() {
	      		changeStatus($(this).attr("href"));
	      		return false;
	});
}


SCRIPT;
$cs=Yii::app()->getClientScript();
// $cs->registerPackage('select2');
$cs->registerScript('select2Init',$script, CClientScript::POS_READY);
?>