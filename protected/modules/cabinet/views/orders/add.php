<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'printer-form',
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'method' => 'post',
	'htmlOptions' => array('role' => 'form'),
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'errorCssClass' => 'has-error',
                'afterValidate'=>'js:function(form,data,hasError){if(hasError){$("#update-loader").hide();}return true;}'
	)
		));
?>
<div style="color:red;">
	<?php echo CHtml::errorSummary($model); ?>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-money"></i> Основные
				</div>
			</div>
			<div class="portlet-body form">
				<div class="form-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="input-icon">
									<?php echo $form->dropDownList($model, 'printer_id', $printers, array(
											'class'=>'form-control',
											'placeholder'=>$model->getAttributeLabel('printer_id'))
										);
									?>
									<?php echo $form->error($model, 'printer_id', array('style' => 'color: #b94a48;')); ?>
								</div>
							</div>
							<div class="form-group">
								<div class="input-icon">
									<?php echo $form->dropDownList($model, 'template_id', $templates, array(
											'class'=>'form-control',
											'placeholder'=>$model->getAttributeLabel('template_id'))
										);
									?>
									<?php echo $form->error($model, 'template_id', array('style' => 'color: #b94a48;')); ?>
								</div>
							</div>
							<div class="form-group">
								<div class="input-icon">
									<?php echo $form->dropDownList($model, 'type', array(
											'instagram'=>'Instagram',
											'vk'=>'Вконтакте',
										), array(
											'class'=>'form-control',
											'placeholder'=>$model->getAttributeLabel('type'))
										);
									?>
									<?php echo $form->error($model, 'type', array('style' => 'color: #b94a48;')); ?>
								</div>
							</div>
							<div class="form-group">
								<div class="input-icon">
									<?php echo $form->textField($model, 'search_query', array('placeholder' => $model->getAttributeLabel('search_query'), 'class' => 'form-control')); ?>
									<?php echo $form->error($model, 'search_query', array('style' => 'color: #b94a48;')); ?>
								</div>
							</div>
							<div class="form-group">
								<div class="input-icon">
									<?php echo $form->textField($model, 'time_limit', array('placeholder' => $model->getAttributeLabel('time_limit'), 'class' => 'form-control')); ?>
									<?php echo $form->error($model, 'time_limit', array('style' => 'color: #b94a48;')); ?>
								</div>
							</div>
							<div class="form-group">
								<div class="input-icon">
									<?php echo $form->textField($model, 'cnt_limit', array('placeholder' => $model->getAttributeLabel('cnt_limit'), 'class' => 'form-control')); ?>
									<?php echo $form->error($model, 'cnt_limit', array('style' => 'color: #b94a48;')); ?>
								</div>
							</div>


						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
        <div class="col-md-6">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                            <i class="fa fa-gift"></i> Настройки печати
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
								<div class="form-group">
									<div class="input-icon">
										<?php echo $form->textField($model->storage, 'cnt_per_file', array('placeholder' => $model->storage->getAttributeLabel('cnt_per_file'), 'class' => 'form-control')); ?>
										<?php echo $form->error($model->storage, 'cnt_per_file', array('style' => 'color: #b94a48;')); ?>
									</div>
								</div>
								<div class="form-group">
									<div class="input-icon">
										<?php echo $form->textField($model->storage, 'cnt_per_user', array('placeholder' => $model->storage->getAttributeLabel('cnt_per_user'), 'class' => 'form-control')); ?>
										<?php echo $form->error($model->storage, 'cnt_per_user', array('style' => 'color: #b94a48;')); ?>
									</div>
								</div>
								<div class="form-group">
									<div class="input-icon">
										<?php echo $form->dropDownList($model->storage, 'page_orientation', array(
											JobStorage::FLIP_ON_LONG_EDGE => 'Альбомная',
											JobStorage::FLIP_ON_SHORT_EDGE => 'Портретная',

										), array(
											'class'=>'form-control',
											'placeholder'=>$model->storage->getAttributeLabel('page_orientation'))
										);
										?>
										<?php echo $form->error($model->storage, 'page_orientation', array('style' => 'color: #b94a48;')); ?>
									</div>
								</div>
								<div class="form-group">
									<div class="input-icon">
										<?php echo $form->dropDownList($model->storage, 'color', array(
											JobStorage::STANDARD_COLOR => 'Стандартная цветная',
											JobStorage::STANDARD_MONOCHROME => 'Стандартная монохромная',

										), array(
											'class'=>'form-control',
											'placeholder'=>$model->storage->getAttributeLabel('color'))
										);
										?>
										<?php echo $form->error($model->storage, 'color', array('style' => 'color: #b94a48;')); ?>
									</div>
								</div>
								<div class="form-group">
									<div class="input-icon">
										<?php echo $form->dropDownList($model->storage, 'media_size', array(
											JobStorage::NA_10X15 => 'Фото 10х15',
											JobStorage::ISO_A4 => 'ИСО А4',

										), array(
											'class'=>'form-control',
											'placeholder'=>$model->storage->getAttributeLabel('media_size'))
										);
										?>
										<?php echo $form->error($model->storage, 'media_size', array('style' => 'color: #b94a48;')); ?>
									</div>
								</div>
								<div class="form-group">
									<div class="input-icon">
										<?php echo $form->dropDownList($model->storage, 'fit_to_page', array(
											'Без полей',
											'Стандартные поля',

										), array(
											'class'=>'form-control',
											'placeholder'=>$model->storage->getAttributeLabel('fit_to_page'))
										);
										?>
										<?php echo $form->error($model->storage, 'fit_to_page', array('style' => 'color: #b94a48;')); ?>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="row">
<div class="col-md-12">
    <div style="text-align: center;" class="well">
        <button type="button" class="btn btn-default btn-lg" onclick="location.href='/cabinet/orders';">Отмена</button>
        <button type="submit" class="btn btn-success btn-lg" onclick="$('#update-loader').show();" style="margin-left: 10px;">Добавить</button>
        <span style="width:43px;margin-left:10px;display: inline-block;"><img id="update-loader" style="display:none;" src="/img/ajax-loader.gif"/></span>
    </div>
</div>
</div>
<?php $this->endWidget();?>