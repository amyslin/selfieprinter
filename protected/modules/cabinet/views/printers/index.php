<div class="row">
      <div class="col-md-12">
        <?php             
            echo CHtml::link('Добавить принтер', array('add'), array('class'=>'btn btn-success btn-sm pull-right'));
        ?>        
      </div>
</div>
<div class="row" style="margin-top: 20px;">
<div class="col-md-12">
<div class="portlet">
<div class="portlet-title">
    <div class="caption">
        <i class="fa fa-print"></i> Принтеры
    </div>
</div>
<div class="portlet-body">
<div class="dataTables_filter pull-right" style="margin-bottom: 20px;">
	<form id="frmOrgSearch" method="GET" class="form-inline" role="form">
			<div class="form-group">
				
			</div>			
			<div class="form-group">
			<button class="btn btn-success" type="submit">
				<i class="fa fa-search"></i>
			</button>
			</div>
	</form>
</div>	
<?php

$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider'=>$provider,	
		'selectableRows' => 0,
		'htmlOptions'=>array('class'=>'table-responsive','style'=>'cursor:pointer;'),                
		'itemsCssClass'=>'table table-bordered table-hover',
		'template'=>'<div class="overlay"></div><div style="padding-bottom:10px;padding-top: 10px; font-size:1.1em;">{summary}</div>{items}{pager}',
		'summaryText'=>'{start}-{end} из {count}',
		'pagerCssClass'=>'pagination',
		'pager'=>array(
			'header'=>'',
			'htmlOptions'=>array('class'=>'pagination'),
		),
		'columns'=>array(			
			array('name'=>'printer_id', 'header'=>'ID'),
			array('name'=>'name', 'header'=>'Название'),
			array('name'=>'address', 'header'=>'Адрес'),
			array('name'=>'c_time', 'header'=>'Дата', 'type'=>'raw', 'value'=>'date("d.m.Y H:i", strtotime($data->c_time))'),
			array('name'=>'type', 'header'=>'Протокол'),
			array(
                'class'=>'CButtonColumn',
                'template' => '{update}',
                'buttons' => array(                                                
				    'update' => array('label' => 'Редактировать','url' => 'Yii::app()->controller->createUrl("printers/update/id/" . $data->printer_id)')
                    )
            )
		)
));
?>
</div>	
</div>	
</div>
</div>
<?php
$script = <<<SCRIPT
$('.select2me').select2();
$('.select2me').change(function(){ $('#frmOrgSearch').submit(); });		
SCRIPT;
$cs=Yii::app()->getClientScript();
$cs->registerPackage('select2');
//$cs->registerScript('select2Init',$script, CClientScript::POS_READY);
?>