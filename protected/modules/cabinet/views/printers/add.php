<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'printer-form',
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'method' => 'post',
	'htmlOptions' => array('role' => 'form'),
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'errorCssClass' => 'has-error',
                'afterValidate'=>'js:function(form,data,hasError){if(hasError){$("#update-loader").hide();}return true;}'
	)
		));
?>
<div style="color:red;">
	<?php echo CHtml::errorSummary($model); ?>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-money"></i> Основные
				</div>	
			</div>
			<div class="portlet-body form">        
				<div class="form-body">          
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">		
								<div class="input-icon">
									<?php echo $form->textField($model, 'name', array('placeholder' => $model->getAttributeLabel('name'), 'class' => 'form-control')); ?>
									<?php echo $form->error($model, 'name', array('style' => 'color: #b94a48;')); ?>
								</div>
							</div>
							<div class="form-group">		
								<div class="input-icon">
									<?php echo $form->textField($model, 'address', array('placeholder' => $model->getAttributeLabel('address'), 'class' => 'form-control')); ?>
									<?php echo $form->error($model, 'address', array('style' => 'color: #b94a48;')); ?>
								</div>
							</div>														
							<div class="form-group">		
								<div class="input-icon">
									<?php echo $form->dropDownList($model, 'type', array('google'=>'google','ipp'=>'ipp'), array('class' => 'form-control')); ?>
									<?php echo $form->error($model, 'type', array('style' => 'color: #b94a48;')); ?>
								</div>
							</div>
							
						</div>    
					</div>	
									
				</div>              
			</div>
		</div>
	</div>
        <div class="col-md-6">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                            <i class="fa fa-gift"></i> Характеристики
                    </div>	
                </div>
                <div class="portlet-body form">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php foreach ($model->storage->getAttributes() as $name=>$val):?>
								<div class="form-group">		
									<div class="input-icon">									
										<?php echo $form->textField($model->storage, $name, array('placeholder' => $model->storage->getAttributeLabel($name), 'class' => 'form-control')); ?>
										<?php echo $form->error($model->storage, $name, array('style' => 'color: #b94a48;')); ?>
									</div>
								</div>
								<?php endforeach;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="row">
<div class="col-md-12">
    <div style="text-align: center;" class="well">
        <button type="button" class="btn btn-default btn-lg" onclick="location.href='/cabinet/printers';">Отмена</button>
        <button type="submit" class="btn btn-success btn-lg" onclick="$('#update-loader').show();" style="margin-left: 10px;">Добавить</button>
        <span style="width:43px;margin-left:10px;display: inline-block;"><img id="update-loader" style="display:none;" src="/img/ajax-loader.gif"/></span>        
    </div>
</div>
</div>
<?php $this->endWidget();?>