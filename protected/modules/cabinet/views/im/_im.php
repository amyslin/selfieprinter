<?php 
$owner  = Yii::app()->controller->owner;
$responder  = Yii::app()->controller->responder;
?>

<li class="<? echo ($data['out']==1)?'out':'in' ?>">
										<img class="avatar img-responsive" alt="" 
										src="<?php echo ($data['out']==1)?$owner['photo_50']:$responder['photo_50']?>">
										<div class="message">
											<span class="arrow">
											</span>
											<a href="#" class="name"><?php echo ($data['out']==1)?$owner['first_name'] . " " .$owner['last_name']:$responder['first_name'] . " " .$responder['last_name']?></a>
											<span class="datetime">
												 <?echo date('d.m.Y H:i', $data['date'])?>
											</span>
											<span class="body">
												 <?=$data['body']?>
											</span>
											
											<?php if(!empty($data['attachments'])): ?>
													<?php $attachments = CJSON::decode($data['attachments']); ?>
												 	<?php foreach ($attachments as $attachment):?>
												 		<?php if($attachment['type'] == 'photo'): ?>
												 		
												 				<img src="<?=$attachment['photo']['photo_604']?>" />
												 		<?php endif; ?>
												 		<?php if($attachment['type'] == 'wall'): ?>
												 				<div>
												 				<span><?=$attachment['wall']['text']?></span>
												 				<div>
												 					<?php if(isset($attachment['wall']['attachments'])):?>
												 					<div>
												 						<?php foreach($attachment['wall']['attachments'] as $att):?>
												 							<?php if ($att['type'] =='photo'):?>
												 								<img src="<?=$att['photo']['photo_604']?>" />
												 							<?php endif;?>
												 						<?php endforeach; ?>
												 					</div>
												 					<?php endif;?>
												 				</div>
												 				
												 				</div>
												 		<?php endif; ?>
												 	<?php endforeach;?>
											<?php endif;?>
										</div>
</li>
