<?php
$helper = new CUrlHelper();
$fields = array(
	'user_ids'=>$data['user_id'] . ',68651428',
	'fields'=>'photo_50,last_seen',
	'access_token'=>'c21989b89279a0424031a40df56ab242e4b48c92c016579ce072c30b4c036806f0475f0390ac903d780df',
	'name_case'=>'Nom',
	'v'=>'5.52'
);
$helper->execute('https://api.vk.com/method/users.get?'.http_build_query($fields), array());
if($helper->isHttpOK()){
	$vk_data = CJSON::decode($helper->content);
}
?>
<div class="dialogs_row">
<?php if (isset($vk_data)):?>
<table cellpadding="0" cellspacing="0" class="dialogs_row_t">
    <tbody><tr>
      <td class="dialogs_photo">
        <img src="<?=$vk_data['response'][0]['photo_50']?>" width="50" height="50">
      </td>
      <td class="dialogs_info">
        <div class="dialogs_user wrapped"><a href="<?php echo Yii::app()->controller->createUrl('dialog', array('id'=>$data['user_id']))?>"><?=$vk_data['response'][0]['first_name']?> <?=$vk_data['response'][0]['last_name']?></a></div>
        <div class="dialogs_date"><?php echo date('d.m.Y H:i', $data['date'])?></div>
      </td>
      <td class="dialogs_msg_contents">
        <div class="dialogs_msg_body  clear_fix" style="opacity: 1;">
        	<?php if (!empty($vk_data['response'][1]['photo_50'])&&!empty($vk_data['response'][0]['photo_50'])):?>
          <img class="dialogs_inline_author fl_l" src="<?php echo ($data['out_m']==1)?$vk_data['response'][1]['photo_50']:$vk_data['response'][0]['photo_50']?>" width="32" height="32">
          <?php else:?>
           <img class="dialogs_inline_author fl_l" src="" width="32" height="32">
          <?php endif;?>
          <?php if(!empty($vk_data['response'][1]['first_name'])):?>
          <div class="dialogs_msg_text wrapped fl_l"><div class="dialogs_chat_title"><?php echo ($data['out_m']==1)?$vk_data['response'][1]['first_name'] . " ".  $vk_data['response'][1]['last_name']:$vk_data['response'][0]['first_name'] . " ".  $vk_data['response'][0]['last_name']?></div><?=$data['last_message']?></div>
          <?php else:?>
          <div class="dialogs_msg_text wrapped fl_l"><div class="dialogs_chat_title">NoName</div><?=$data['last_message']?></div>
          <?php endif;?>
        </div>
      </td>
      <td class="dialogs_unread_td">
        <div class="dialogs_unread unshown" ><?=$data['not_read']?></div>
      </td>
    </tr>
  </tbody>
 </table>
 <?php endif;?>
	
</div>
