<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'template-form',
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'method' => 'post',
	'htmlOptions' => array('role' => 'form', 'enctype'=>'multipart/form-data'),
	'clientOptions' => array(
		'validateOnSubmit' => false,
		'errorCssClass' => 'has-error',
                'afterValidate'=>'js:function(form,data,hasError){if(hasError){$("#update-loader").hide();}return true;}'
	)
		));
?>
<div style="color:red;">
	<?php echo CHtml::errorSummary($model); ?>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-money"></i> Загрузка шаблона
				</div>
			</div>
			<div class="portlet-body form">
				<div class="form-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="input-icon">
									<?php echo $form->textField($model, 'name', array('placeholder' => $model->getAttributeLabel('name'), 'class' => 'form-control')); ?>
									<?php echo $form->error($model, 'name', array('style' => 'color: #b94a48;')); ?>
								</div>
							</div>
							<div class="form-group">
								<div class="input-icon">
									<?php echo $form->textField($model, 'description', array('placeholder' => $model->getAttributeLabel('description'), 'class' => 'form-control')); ?>
									<?php echo $form->error($model, 'description', array('style' => 'color: #b94a48;')); ?>
								</div>
							</div>
							<div class="form-group">
								<div class="input-icon">
									<?php echo $form->fileField($model, 'data', array('placeholder' => $model->getAttributeLabel('data'), 'class' => 'form-control')); ?>
									<?php echo $form->error($model, 'data', array('style' => 'color: #b94a48;')); ?>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
<div class="col-md-12">
    <div style="text-align: center;" class="well">
        <button type="button" class="btn btn-default btn-lg" onclick="location.href='/cabinet/templates';">Отмена</button>
        <button type="submit" class="btn btn-success btn-lg" style="margin-left: 10px;">Добавить</button>
        <span style="width:43px;margin-left:10px;display: inline-block;"><img id="update-loader" style="display:none;" src="/img/ajax-loader.gif"/></span>
    </div>
</div>
</div>
<?php $this->endWidget();?>