<div class="row">
      <div class="col-md-12">
        <?php
            echo CHtml::link('Добавить шаблон', array('add'), array('class'=>'btn btn-success btn-sm pull-right'));
        ?>
      </div>
</div>
<div class="row" style="margin-top: 20px;">
<div class="col-md-12">
<div class="portlet">
<div class="portlet-title">
    <div class="caption">
        <i class="fa fa-print"></i> Шаблоны
    </div>
</div>
<div class="portlet-body">
<div class="dataTables_filter pull-right" style="margin-bottom: 20px;">
	<form id="frmOrgSearch" method="GET" class="form-inline" role="form">
			<div class="form-group">

			</div>
			<div class="form-group">
			<button class="btn btn-success" type="submit">
				<i class="fa fa-search"></i>
			</button>
			</div>
	</form>
</div>

<?php

$this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$provider,
// 		'selectableRows' => 0,
// 		'afterAjaxUpdate'=>'tableOnUpdate',
// 		'htmlOptions'=>array('class'=>'table-responsive','style'=>'cursor:pointer;'),
		'itemView'=>'_item',
		'ajaxUpdate' => true,
		'itemsCssClass'=>'row thumbnails',
		'template'=>'<div class="overlay"></div><div style="padding-bottom:10px;padding-top: 10px; font-size:1.1em;">{summary}</div>{items}{pager}<div style="clear: both;"/>',
		'summaryText'=>'{start}-{end} из {count}',
		'pagerCssClass'=>'pagination',
		'pager'=>array(
			'header'=>'',
			'htmlOptions'=>array('class'=>'pagination'),
		),
));
?>

</div>
</div>
</div>
</div>

<?php

$cs=Yii::app()->getClientScript();
$cs->registerCssFile('/css/pages/about-us.css');
// $cs->registerPackage('select2');
// $cs->registerScript('init',$script, CClientScript::POS_READY);
?>