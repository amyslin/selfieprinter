<div class="col-md-4">
<div class="portlet">
	<div class="portlet-title">
		<div class="caption"><?php echo $data->name;?></div>
		<div class="actions">
			<?php echo CHtml::link('<i class="fa fa-pencil-square"></i>', Yii::app()->controller->createUrl('update', array('id'=>$data->template_id)), array('class'=>'btn btn-info btn-sm', 'title'=>'Редактировать'));?>
			<?php echo CHtml::link('<i class="fa fa-times"></i>', Yii::app()->controller->createUrl('delete', array('id'=>$data->template_id)), array('class'=>'btn btn-info btn-sm', 'title'=>'Удалить'));?>
		</div>
	</div>
	<div class="portlet-body">
	<?php
		$item = PtJobItem::model()->with(array('job'))->find(array('order'=>'rand()'));
		$userData = CJSON::decode($item->source_data);
		$params = array(
				'tag'=>array(
					'type'=>'text',
					'value'=>'#'.$item->job->search_query,
				),
				'image'=>array(
					'type'=>'image',
					'value'=>$item->source_file,
				),
		);
		if (isset($userData['user']['profile_picture']))
			$params['userpic'] = array(
				'type'=>'image',
				'value'=>$userData['user']['profile_picture']
			);
		
		if (isset($userData['user']['username']))
			$params['username'] = array(
					'type'=>'text',
					'value'=>$userData['user']['username']
			);
		if (isset($userData['userpic']))
			$params['userpic'] = array(
					'type'=>'image',
					'value'=>$userData['userpic']
			);
		
		if (isset($userData['username']))
			$params['username'] = array(
					'type'=>'text',
					'value'=>$userData['username']
			);
		$src = 'data:image/png;base64,'. base64_encode($data->renderTemplate($params));
		echo CHtml::image($src, '', array('class'=>'img-responsive'));

	?>
	<p><?=$data->description?></p>
	</div>
</div>
</div>