<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'user-form',
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'method' => 'post',
	'htmlOptions' => array('role' => 'form','class'=>'form-horizontal'),
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'errorCssClass' => 'has-error',
                'afterValidate'=>'js:function(form,data,hasError){if(hasError){$("#update-loader").hide();}return true;}'
	)
		));
?>
<div style="color:red;">
	<?php echo CHtml::errorSummary($model); ?>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-user"></i> Настройки пользователя
				</div>
			</div>
			<div class="portlet-body form">
				<div class="form-body">
					<div class="row">
						<div class="col-md-12">

							<div class="form-group">
								<label class="col-md-3 control-label"><?php echo $model->getAttributeLabel('username'); ?></label>
								<div class="col-md-9">
									<div class="input-icon">
										<i class="fa fa-phone"></i>
										<?php echo $form->textField($model, 'user_name', array('placeholder' => $model->storage->getAttributeLabel('user_name'), 'class' => 'form-control')); ?>
										<?php echo $form->error($model, 'user_name', array('style' => 'color: #b94a48;')); ?>
									</div>
								</div>
                            </div>
							<div class="form-group">
								<label class="col-md-3 control-label"><?php echo $model->getAttributeLabel('user_password'); ?></label>
								<div class="col-md-9">
									<div class="input-icon">
										<i class="fa fa-cogs"></i>
										<?php echo $form->passwordField($model, 'user_password', array('placeholder' => $model->getAttributeLabel('user_password'), 'class' => 'form-control')); ?>
										<?php echo $form->error($model, 'user_password', array('style' => 'color: #b94a48;')); ?>
									</div>
								</div>
                            </div>
							<div class="form-group">
								<label class="col-md-3 control-label"><?php echo $model->getAttributeLabel('password_confirm'); ?></label>
								<div class="col-md-9">
									<div class="input-icon">
										<i class="fa fa-cogs"></i>
										<?php echo $form->passwordField($model, 'password_confirm', array('placeholder' => $model->getAttributeLabel('password_confirm'), 'class' => 'form-control')); ?>
										<?php echo $form->error($model, 'password_confirm', array('style' => 'color: #b94a48;')); ?>
									</div>
								</div>
                            </div>
							<div class="form-group">
								<label class="col-md-3 control-label"><?php echo $model->getAttributeLabel('active'); ?></label>
								<div class="col-md-9">
									<div class="input-icon">
										<?php echo $form->checkBox($model, 'active'); ?>
										<?php echo $form->error($model, 'active', array('style' => 'color: #b94a48;')); ?>
									</div>
								</div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
            <div class="portlet">
                <div class="portlet-title">
				<div class="caption">
					<i class="fa fa-book"></i> Данные пользователя
				</div>
		</div>
                <div class="portlet-body form">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php foreach ($model->storage->getAttributes() as $name=>$val):?>
                                                            <div class="form-group">
                                                                    <label class="col-md-3 control-label"><?php echo $model->storage->getAttributeLabel($name); ?></label>
                                                                    <div class="col-md-9">
                                                                            <div class="input-icon">
                                                                                    <i class="fa fa-user"></i>
                                                                                    <?php echo $form->textField($model->storage, $name, array('placeholder' => $model->storage->getAttributeLabel($name), 'class' => 'form-control')); ?>
                                                                                    <?php echo $form->error($model->storage, $name, array('style' => 'color: #b94a48;')); ?>
                                                                            </div>
                                                                    </div>
                                                            </div>
                                <?php endforeach;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	</div>
	<div class="col-md-6">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs"></i> Права доступа
				</div>
			</div>
			<div class="portlet-body form">
				<div class="form-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="col-md-3 control-label"><?php echo $model->getAttributeLabel('auth_items'); ?></label>
								<div class="col-md-9">
									<div class="input-icon">
										<?php echo $form->checkBoxList($model, 'auth_items', $auth_items); ?>
										<?php echo $form->error($model, 'auth_items', array('style' => 'color: #b94a48;')); ?>
									</div>
								</div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs"></i> Подключение аккаунтов
				</div>
			</div>
			<div class="portlet-body form">
				<div class="form-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="col-md-9">
									<a href="" class="btn btn-info disabled"><i class="fa fa-instagram"></i></a> Instagram
								</div>
<!-- 								<a href="#" data-original-title="instagram" class="social-icon instagram"></a> -->
								<div class="col-md-3">
									<input type="checkbox" data="http://selfieprinter.ru<?php echo $this->createUrl(Yii::app()->request->requestUri, array('service'=>'instagram'))?>" <?php echo (!empty($profiles['instagram'])&&!empty($profiles['instagram']->token))?'checked="checked" ':''; ?> class="toggle pull-right instagram"/>
								</div>
                            </div>
                            <div class="form-group">
								<div class="col-md-9">
									<a href="" class="btn btn-info disabled"><i class="fa fa-facebook"></i></a>
									 Facebook
								</div>
<!-- 								<a href="#" data-original-title="instagram" class="social-icon instagram"></a> -->
								<div class="col-md-3">
									<input type="checkbox" disabled="disabled"  class="toggle pull-right"/>
								</div>
                            </div>
                            <div class="form-group">
								<div class="col-md-9">
									<a href="" class="btn btn-info disabled"><i class="fa fa-vk"></i></a> Vkontakte
								</div>
<!-- 								<a href="#" data-original-title="instagram" class="social-icon instagram"></a> -->
								<div class="col-md-3">
									<input type="checkbox" data="http://selfieprinter.ru<?php echo $this->createUrl(Yii::app()->request->requestUri, array('service'=>'vk'))?>" <?php echo (!empty($profiles['vk'])&&!empty($profiles['vk']->token))?'checked="checked" ':''; ?> class="toggle pull-right vk"/>
								</div>
                            </div>
                           	<div class="form-group">
								<div class="col-md-9">
									<a href="" class="btn btn-info disabled"><i class="fa fa-google"></i></a> Google
								</div>
<!-- 								<a href="#" data-original-title="instagram" class="social-icon instagram"></a> -->
								<div class="col-md-3">
									<input type="checkbox" data="http://selfieprinter.ru<?php echo $this->createUrl(Yii::app()->request->requestUri, array('service'=>'google'))?>" <?php echo (!empty($profiles['google'])&&!empty($profiles['google']->token))?'checked="checked" ':''; ?> class="toggle pull-right google"/>
								</div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<div class="row">
<div class="col-md-12">
    <div style="text-align: center;" class="well">
        <button type="button" class="btn btn-default btn-lg" onclick="location.href='/crm/user/index';">Отмена</button>
        <button type="submit" class="btn btn-success btn-lg" onclick="$('#update-loader').show();" style="margin-left: 10px;">Сохранить</button>
    </div>
</div>
</div>
<?php $this->endWidget();?>
<?php
$url = Yii::app()->assetManager->publish(Yii::app()->basePath . '/assets', false, -1, Yii::app()->assetManager->linkAssets?false:YII_DEBUG);
$cs = Yii::app()->clientScript;

$js="$('.toggle').bootstrapSwitch();\n";

foreach ($services as $name => $service) {
	$args = $service->jsArguments;
	$args['id'] = $service->id;
	$js .= '$(".' . $service->id . '").eauth(' . json_encode($args) . ');' . "\n";
}

$cs->registerScriptFile($url . '/eauth/auth.js', CClientScript::POS_END);
$cs->registerScript('eauth-services', $js, CClientScript::POS_READY);
?>