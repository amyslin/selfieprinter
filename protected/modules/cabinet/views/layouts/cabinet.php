<?php
$cs=Yii::app()->clientScript;
$cs->registerPackage('font-awesome');
$cs->registerPackage('bootstrap');
$cs->registerPackage('uniform');
// $cs->registerPackage('blockui');
$cs->registerPackage('simple-line-icons');
$cs->registerPackage('bootstrap-toastr');
$cs->registerPackage('bootstrap-switch');
$cs->registerCssFile('/css/style-conquer.css');
$cs->registerCssFile('/css/plugins.css');
$cs->registerCssFile('/css/style.css');
$cs->registerCssFile('/css/style-responsive.css');
$cs->registerCssFile('/css/pages/tasks.css');
$cs->registerCssFile('/css/themes/default.css');
$cs->registerCssFile('/css/custom.css');
$script = <<< SCRIPT
toastr.options = {
  "closeButton": true,
  "debug": false,
  "positionClass": "toast-top-right",
  "showDuration": "1000",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};
SCRIPT;
foreach (Yii::app()->user->getFlashes() as $key => $message) {
	$script .= "toastr.$key('$message')\n";
}
$cs->registerScript('toastr', $script, CClientScript::POS_READY);
?>


<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>МДМ | Личный кабинет - <?php echo $this->pageTitle; ?></title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <meta name="MobileOptimized" content="320">
   <link rel="shortcut icon" href="/favicon.ico" />
</head>
<!-- END HEAD -->
<div class="page-header-fixed">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-fixed-top">
  <!-- BEGIN TOP NAVIGATION BAR -->
  <div class="header-inner">
    <!-- BEGIN LOGO -->
    <?php $this->widget('LMenu', array(
    	'items'=>$this->getTopMenu(),
 		'htmlOptions' => array('class'=>'nav navbar-nav pull-right'),
                'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
 		'firstItemCssClass' => 'start',
 		'activeCssClass' => 'active',
	));?>
    <!-- END LOGO -->
    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
    <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
    <img src="/img/menu-toggler.png" alt="" />
    </a>
    <!-- END RESPONSIVE MENU TOGGLER -->
    <!-- BEGIN TOP NAVIGATION MENU -->

    <!-- END TOP NAVIGATION MENU -->
  </div>
  <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
	<div class="page-sidebar-wrapper">

  <!-- MAIN MENU GOES HERE.-->
 	<div class="page-sidebar navbar-collapse collapse">
 	<?php $this->widget('LMenu', array(
    	'items'=>$this->getMenu(),
 		'htmlOptions' => array('class'=>'page-sidebar-menu'),
 		'submenuHtmlOptions'=>array('class'=>'sub-menu'),
 		'firstItemCssClass' => 'start',
 		'activeCssClass' => 'active',
	));?>
	</div>
	</div>
</div>
<!-- END SIDEBAR -->

<!-- BEGIN PAGE -->
<div class="page-content-wrapper">
<div class="page-content-wrapper">
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <?php echo $content; ?>
</div>
</div>
</div>
</div>
<!-- END PAGE -->
</div>