<?php
Yii::import('application.modules.cabinet.components.*');
Yii::import('application.modules.cabinet.widgets.*');
Yii::import('application.forms.storage.*');
Yii::import('application.modules.cabinet.forms.*');
class CabinetModule extends CWebModule {
	public function init()
	{
		$this->defaultController = 'orders';
	}
}