<?php
class ControllerCabinet extends CController {
	public $title;
	public $subtitle;
	public $perpage;
	public $broad = array();

	public $user;

	public function init() {
		parent::init();
		$this->layout='cabinet';
		if (isset($_GET['perpage']) && is_numeric($_GET['perpage'])){
			Yii::app()->session['perpage'] = $_GET['perpage'];
		}
		$this->user = PtUser::model()->with('profiles')->findByPk(Yii::app()->user->id);
		$this->perpage = isset(Yii::app()->session['perpage']) ? Yii::app()->session['perpage'] : 20;
	}

	public function filters()
	{
		return array(
				'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
				array('allow','users'=>array('@'),'roles'=>array('admin', 'cabinet')),
				array('deny','users' => array('*')),
		);

	}

	public function getMenu() {

		$menu = array();
		$menu[] = array('label'=>'Ордеры на печать', 'url' => array('orders/index'), 'icon'=>'camera-retro', 'active'=>$this->id=='orders'?true:false);
		$menu[] = array('label'=>'Принтеры', 'url' => array('printers/index'), 'icon'=>'print', 'active'=>$this->id=='printers'?true:false);
		$menu[] = array('label'=>'Шаблоны', 'url' => array('templates/index'), 'icon'=>'file', 'active'=>$this->id=='templates'?true:false);


		return $menu;

	}

	public function getTopMenu() {
		$dropDownItems = array();
		//$dropDownItems[] = array('label'=>'Профиль', 'url'=>array('/cabinet/workers/view/id/' . $this->user->user_id), 'icon'=>'user');
		//if (Yii::app()->user->checkAccess('admin')){
		//    $dropDownItems[] = array('label'=>'Админ-панель', 'url' => array('/cabinet'), 'icon'=>'cogs');
		//}
		$dropDownItems[] = array('label'=>'Настройки', 'url' => array('user/settings'), 'icon'=>'cogs');
		$dropDownItems[] = array('label'=>'Выход', 'url' => array('/auth/logout'), 'icon'=>'power-off');

		return array(
				//array('class' => 'BadgeMenuElement', 'url'=>'#', 'icon'=>'warning', 'style'=>'warning','itemOptions'=>array('class'=>'dropdown'), 'htmlOptions'=>array('class'=>'dropdown-toggle', 'data-toggle'=>'dropdown', 'data-hover'=>'dropdown', 'data-close-others'=>'true')),
				//array('class' => 'BadgeMenuElement', 'url'=>'#', 'icon'=>'envelope', 'style'=>'info','itemOptions'=>array('class'=>'dropdown'), 'htmlOptions'=>array('class'=>'dropdown-toggle', 'data-toggle'=>'dropdown', 'data-hover'=>'dropdown', 'data-close-others'=>'true')),
				array('itemOptions' => array('class'=>'devider')),
				array('class'=>'UserMenuElement', 'url'=>'#', 'itemOptions'=>array('class'=>'user dropdown'), 'htmlOptions'=>array('class'=>'dropdown-toggle', 'data-toggle'=>'dropdown', 'data-hover'=>'dropdown', 'data-close-others'=>'true'),
						'items'=> $dropDownItems,
				)
		);
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']))
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}