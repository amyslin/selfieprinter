<?php
class BadgeMenuElement extends CComponent {
	
	/**
	 * @var string Урл
	 */
	public $url;
	
	/**
	 * @var string Иконка
	 */
	public $icon;
	
	/**
	 * @var string Стиль success warning error info
	 */
	public $style;
	
	/**
	 * @var string Описание ссылки
	 */
	public $label;
	
	/**
	 * @var integer Описание ссылки
	 */
	public $counter = 0;
	
	/**
	 * @var array HTML опции тэга
	 */
	public $htmlOptions=array();
	
	/**
	 * @var array HTML опции элемента
	 */
	public $itemOptions=array();
	
	/**
	 * @var boolean Активное
	 */
	public $active=true;
	
	/**
	 * @var array Элементы подменю
	 */
	public $items=array();
	
	/**
	 * @var LMenu HTML опции тэга
	 */
	public $lMenu;
	
	public function __construct($lmenu) {
		$this->lMenu = $lmenu;
	}
	
	public function init() {
		$class[] = 'badge';
		$label = '';
		if (!empty($this->icon))
		$label .= '<i class="fa fa-'.$this->icon.'">&nbsp;</i>';
		
		if (!empty($this->style))
			$class[] = 'badge-' . $this->style;
		
		$label .= '<span class="' . implode(' ', $class) .'">' . $this->counter . '</span>';
		return CHtml::link($label, $this->url, $this->htmlOptions);
	}
}