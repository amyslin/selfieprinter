<?php
Yii::import('zii.widgets.CMenu');
class LMenu extends CMenu {
	public $icon;
	
	public $tuggle = false;
	
	public function init() {
		parent::init();
	}
	
	protected function renderMenu($items)
	{
		if(count($items))
		{
			echo CHtml::openTag('ul',$this->htmlOptions)."\n";
			if ($this->tuggle == true) {
			echo '<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<div class="clearfix">
					</div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>';
			}
			$this->renderMenuRecursive($items);
			echo CHtml::closeTag('ul');
		}
	}
	
	/**
	 * Recursively renders the menu items.
	 * @param array $items the menu items to be rendered recursively
	 */
	protected function renderMenuRecursive($items)
	{
		$count=0;
		$n=count($items);
		foreach($items as $item)
		{
			$count++;
			$options=isset($item['itemOptions']) ? $item['itemOptions'] : array();
			$class=array();
			if($item['active'] && $this->activeCssClass!='')
				$class[]=$this->activeCssClass;
			if($count===1 && $this->firstItemCssClass!==null)
				$class[]=$this->firstItemCssClass;
			if($count===$n && $this->lastItemCssClass!==null)
				$class[]=$this->lastItemCssClass;
			if($this->itemCssClass!==null)
				$class[]=$this->itemCssClass;
			if($class!==array())
			{
				if(empty($options['class']))
					$options['class']=implode(' ',$class);
				else
					$options['class'].=' '.implode(' ',$class);
			}
			
			if (in_array('devider', $class)) {
				echo '<li class="devider">&nbsp;</li>';
				continue;
			}
				
			echo CHtml::openTag('li', $options);
	
			$menu=$this->renderMenuItem($item);
			if(isset($this->itemTemplate) || isset($item['template']))
			{
				$template=isset($item['template']) ? $item['template'] : $this->itemTemplate;
				echo strtr($template,array('{menu}'=>$menu));
			}
			else
				echo $menu;
	
			if(isset($item['items']) && count($item['items']))
			{
				echo "\n".CHtml::openTag('ul',isset($item['submenuOptions']) ? $item['submenuOptions'] : $this->submenuHtmlOptions)."\n";
				$this->renderMenuRecursive($item['items']);
				echo CHtml::closeTag('ul')."\n";
			}
	
			echo CHtml::closeTag('li')."\n";
		}
	}
	
	protected function renderMenuItem($item) {
		
		if (isset($item['class'])) {
			$element = Yii::createComponent($item, $this);
			return $element->init();
		}
		if (isset($item['icon'])) {
			$item['label'] = '<i class="fa fa-'.$item['icon'].'"></i><span class="title">' .$item['label'] . '</span>';
		}
		if (isset($item['url']))
			return CHtml::link($item['label'], $item['url'], isset($item['linkOptions']) ? $item['linkOptions'] : array());
		else 
			return $item['label'];
	}
}