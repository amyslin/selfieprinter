<?php
class UserMenuElement extends CComponent {

	/**
	 * @var string Урл
	 */
	public $url;

	/**
	 * @var string Описание ссылки
	 */
	public $label;

	/**
	 * @var array HTML опции тэга
	 */
	public $htmlOptions=array();

	/**
	 * @var array HTML опции элемента
	 */
	public $itemOptions=array();

	/**
	 * @var boolean Активное
	 */
	public $active=true;

	/**
	 * @var array Элементы подменю
	 */
	public $items=array();

	/**
	 * @var LMenu HTML опции тэга
	 */
	public $lMenu;

	/**
	 * @var CWebUser Пользователь
	 */
	public $user;


	public function __construct($lmenu) {
		$this->user = Yii::app()->user;
		$this->lMenu = $lmenu;
	}

	public function init() {
                $user = PtUser::model()->findByPk(Yii::app()->user->id);
                if(!empty($user->storage)){
                    $this->label = $user->storage['last_name'].' '.$user->storage['first_name'].' '.$user->storage['patronymic'];
                }else{
                    $this->label = $this->user->name;
                }
		$label = '<span class="username">';
		$label .= $this->label;
		$label .= '</span>';
		$label .= '<i class="fa fa-angle-down"></i>';
		return CHtml::link($label, $this->url, $this->htmlOptions);
	}
}