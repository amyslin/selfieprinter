<?php
class TemplateForm extends CFormModel {
	public $name;
	public $data;
	public $description;

	public function rules() {
		return array(
				array('name, data', 'required'),
				array('data', 'file', 'types'=>'svg, xml'),
				array('name,description', 'safe'),
		);
	}

	public function attributeLabels() {
		return array(
			'name'=>'Имя',
			'description'=>'Описание',
			'data'=>'Файл шаблона',
		);
	}
}