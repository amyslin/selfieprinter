<?php

class UserForm extends CFormModel
{
	public $user_name;
// 	public $org_id;
	public $user_password;
	public $password_confirm;
	public $active;
	public $secret;
	public $auth_items;
        public $shop_id;
        private $_storage;

	public function rules()
	{
		return array(
                    array('user_name','required'),
                    array('user_name','unique','on'=>'create','allowEmpty'=>false, 'className'=>'PtUser', 'attributeName'=>'user_name'),
//                     array('org_id','numerical','min'=>1,'tooSmall'=>'Выберите организацию','integerOnly'=>true),
                    array('user_password,password_confirm','required','on'=>'create'),
                    array('user_password,password_confirm','safe','on'=>'update'),
                    array('password_confirm', 'compare', 'compareAttribute'=>'user_password'),
                    array('active','boolean','allowEmpty'=>true),
                    array('secret','safe'),
                    array('auth_items','type','type'=>'array','allowEmpty'=>false),
//                     array('shop_id','numerical','integerOnly'=>true)
        );
	}

	public function attributeLabels()
	{
		return array(
			'user_name'=>'Логин',
			'org_id'=>'Организация',
			'user_password'=>'Пароль',
			'password_confirm'=>'Повторите пароль',
			'active'=>'Активен',
// 			'secret'=>'Ключ',
			'auth_items'=>'Роли',
// 			'shop_id'=>'Торговая точка'
		);
	}

        public function setStorage($value) {
		if (is_array($value)) {
			if (empty($this->_storage))
				$this->_storage = new UserStorageForm();
			$this->_storage->attributes = $value;
			return $this->_storage;
		}
		return null;
	}

	public function getStorage() {
		if (empty($this->_storage))
			$this->_storage = new UserStorageForm();
		return $this->_storage;
	}

}