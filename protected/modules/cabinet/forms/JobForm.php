<?php
class JobForm extends CFormModel {
	public $type;
	public $time_limit;
	public $cnt_limit;
	public $search_query;
	public $printer_id;
	public $template_id;
	public $job_status;

	private $_settings_storage;

	public function rules() {
		return array(
				array('type,printer_id,search_query,template_id', 'required'),
				array('type', 'in', 'range'=>array('vk','instagram')),
				array('time_limit,cnt_limit', 'numerical', 'allowEmpty'=>false, 'integerOnly'=>true, 'min'=>0),
				array('printer_id', 'exist', 'className'=>'PtPrinter', 'attributeName'=>'printer_id'),
				array('search_query', 'safe'),
				array('job_status', 'in', 'range'=>array('NEW', 'RUN', 'PAUSED', 'FINISHED')),
				array('storage', 'validateForm'),
		);
	}

	public function attributeLabels() {
		return array(
				'type'=>'Тип',
				'time_limit'=>'Лимит по времени (минуты)',
				'cnt_limit'=>'Лимит по количеству листов',
				'search_query'=>'Хэш тэг',
		);
	}

	public function setStorage($value) {
		if (is_array($value)) {
			if (empty($this->_settings_storage))
				$this->_settings_storage = new JobStorage();
			$this->_settings_storage->attributes = $value;
			return $this->_settings_storage;
		}
		return null;
	}

	public function getStorage() {
		if (empty($this->_settings_storage))
			$this->_settings_storage = new JobStorage();
		return $this->_settings_storage;
	}


	public function validateForm($attribute,$params)
	{
		$form=$this->$attribute;
		if($form instanceof CFormModel){
			if(!$form->validate())
				$this->addError($attribute, $form->errors);
		}
	}



}