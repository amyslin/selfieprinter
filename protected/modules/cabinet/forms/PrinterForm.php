<?php
class PrinterForm extends CFormModel {
	public $name;
	public $type;
	public $address;
	public $geo_lat;
	public $geo_lng;
	
	private $_settings_storage;
	
	public function rules() {
		return array(
			array('name, type', 'required'),
			array('type', 'in', 'range'=>array('ipp','google')),
			array('address,geo_lat,geo_lng', 'safe'),
			array('storage', 'validateForm'),
		);
	}
	
	public function attributeLabels() {
		return array(
				'name'=>'Название',
				'type'=>'Тип',
				'address'=>'Адрес установки',
		);
	}
	
	public function setStorage($value) {
		if (is_array($value)) {
			if (empty($this->_settings_storage))
				$this->_settings_storage = new PrinterStorage();
			$this->_settings_storage->attributes = $value;
			return $this->_settings_storage;
		}
		return null;
	}
	
	public function getStorage() {
		if (empty($this->_settings_storage))
			$this->_settings_storage = new PrinterStorage();
		return $this->_settings_storage;
	}
	
	
	public function validateForm($attribute,$params)
	{
		$form=$this->$attribute;
		if($form instanceof CFormModel){
			if(!$form->validate())
				$this->addError($attribute, $form->errors);
		}
	}
}