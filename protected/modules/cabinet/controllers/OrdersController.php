<?php
class OrdersController extends ControllerCabinet {


	public function actionIndex() {
		array_push($this->broad, array('title'=>'Новый ордер', 'url'=>array('#')));
		$criteria = new CDbCriteria();
		$criteria->with = array('user', 'printer');
		$criteria->order = 't.c_time DESC';

		$provider = new CActiveDataProvider('PtJob', array(
		 		'criteria'=>$criteria,
				'pagination' =>array(
						'pageSize'=>$this->perpage,
				),
		));
		if (isset($_GET['id'])&&isset($_GET['status'])) {
			if (Yii::app()->request->isAjaxRequest) {
				$job = PtJob::model()->findByPk($_GET['id']);
				if (empty($job)) {
					echo CJSON::encode(array(
						'status'=>'ERROR',
						'message'=>'Missed id'
					));
					Yii::app()->end();
				}
				$job->job_status = $_GET['status'];
				if($job->save(false)) {
					echo CJSON::encode(array(
						'status'=>'OK'
					));
					Yii::app()->end();
				}

			}
		}

		$this->render('index', array('provider'=>$provider));
	}

	public function actionAdd() {
		array_push($this->broad, array('title'=>'Новый принтер', 'url'=>array('#')));
		$model = new JobForm();
		$printers = PtPrinter::model()->findAll();
		$printersList = array();
		foreach ($printers as $printer)
			$printersList[$printer->printer_id] = $printer->name;
		if (isset($_POST['JobForm'])&&isset($_POST['JobStorage'])) {
			$model->setAttributes($_POST['JobForm']);
			$model->setStorage($_POST['JobStorage']);
			$this->performAjaxValidation($model);
			$this->performAjaxValidation($model->storage);
			if ($model->validate()) {


				$job = new PtJob();
				$job->setAttributes($_POST['JobForm'], false);
				$job->storage = $model->storage->getAttributes();
				$job->user_id = $this->user->user_id;

				if($job->save(false)) {
					$this->redirect('index');
				}
			}
		}
		$templates = PtTemplate::model()->findAll();
		$tempList = array();
		foreach ($templates as $template)
			$tempList[$template->template_id] = $template->name;
		$this->render('add', array('model'=>$model, 'printers'=>$printersList, 'templates'=>$tempList));

	}

	public function actionItems($id) {
		array_push($this->broad, array('title'=>'Новый принтер', 'url'=>array('#')));
		$criteria = new CDbCriteria();
		$criteria->condition = 't.job_id = :jid AND t.source_file IS NOT NULL';
		$criteria->order = 't.c_time DESC';
		$criteria->with = array('job');
		$criteria->params = array(
			'jid'=>$id,
		);
		$provider = new CActiveDataProvider('PtJobItem', array(
				'criteria'=>$criteria,
				'pagination' =>array(
						'pageSize'=>$this->perpage,
				),
		));

		$this->render('items', array('provider'=>$provider));
	}

	public function actionUpdate($id) {
		array_push($this->broad, array('title'=>'Редактировать', 'url'=>array('#')));
		$job = PtJob::model()->findByPk($id);
		if (empty($job))
			throw new CHttpException(403, 'Ордер не найден.');

		$printers = PtPrinter::model()->findAll();
		$printersList = array();
		foreach ($printers as $printer)
			$printersList[$printer->printer_id] = $printer->name;
		$model = new JobForm();
		$model->setAttributes($job->getAttributes());
		$model->storage->setAttributes($job->storage);

		if (isset($_POST['JobForm'])&&isset($_POST['JobStorage'])) {
			$model->setAttributes($_POST['JobForm']);
			$model->setStorage($_POST['JobStorage']);
			$this->performAjaxValidation($model);
			$this->performAjaxValidation($model->storage);
			if ($model->validate()) {

				$job->setAttributes($_POST['JobForm'], false);
				$job->storage = $model->storage->getAttributes();
				$job->user_id = $this->user->user_id;

				if($job->save(false))
					Yii::app()->user->setFlash('success', 'Изменения успешно сохранены');

			}
		}
		$templates = PtTemplate::model()->findAll();
		$tempList = array();
		foreach ($templates as $template)
			$tempList[$template->template_id] = $template->name;
		$this->render('update', array('model'=>$model, 'printers'=>$printersList, 'templates'=>$tempList));


	}

	public function renderJobStatus($data) {
		switch ($data->job_status) {
			case 'NEW':
				return CHtml::tag('span', array('class'=>'label label-warning'), 'Новый');
			break;
			case 'RUN':
				return CHtml::tag('span', array('class'=>'label label-success'), 'В работе');
			break;
			case 'PAUSED':
				return CHtml::tag('span', array('class'=>'label label-warning'), 'Пауза');
			break;
			case 'FINISHED':
				return CHtml::tag('span', array('class'=>'label label-default'), 'Завершена');
			break;
		}
	}
}