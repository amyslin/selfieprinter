<?php
class UserController extends ControllerCabinet {

	public function actionSettings() {
		if(isset($_GET['service'])){
			$service = Yii::app()->eauth->getIdentity($_GET['service']);
			try {
				if ($service->authenticate()) {
					//var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes());
// 					$profiles = $this->user->profiles;

					$profile = PtProfile::model()->findByAttributes(array('user_id'=>$this->user->user_id, 'service'=>$_GET['service']));
					if(empty($profile)) {
						$profile = new PtProfile;
						$profile->user_id = $this->user->user_id;
						$profile->service = strtolower($_GET['service']);
						$profile->save(false);
					}

					if ($service->isAuthenticated && !empty($service->token->access_token)) {
						$token = $profile->token;
						if(empty($token)) {
							$token = new OAuthToken;
							$token->access_token = $service->token->access_token;
							$token->profile_id = $profile->profile_id;
							if(!empty($service->token->refresh_token))
								$token->refresh_token = $service->token->refresh_token;
							if (!empty($service->token->expires_in))
								$token->expires_in = $service->token->expires_in;
							$token->expired = 0;
							$token->save(false);
						}
						elseif($token->access_token != $service->token->access_token) {
							$token->expired = 1;
							$token->save(false);
							$token = new OAuthToken;
							$token->access_token = $service->token->access_token;
							if(!empty($service->token->refresh_token))
								$token->refresh_token = $service->token->refresh_token;
							if (!empty($service->token->expires_in))
								$token->expires_in = $service->token->expires_in;
							$token->profile_id = $profile->profile_id;
							$token->expired = 0;
							$token->save(false);
							$profile->refresh();
						}
					}
// 					Yii::app()->end();

					$service->redirect($this->createUrl(''));

					//                     $identity->email = $model->email;
					// successful authentication

				}
			}
			catch (EAuthException $e) {
				// save authentication error to session
				Yii::app()->user->setFlash('error', 'EAuthException: '.$e->getMessage());

				print_r($e);
				Yii::app()->end();
				// close popup window and redirect to cancelUrl
// 				$service->redirect();
			}
		}

		$user = $this->user;
		$profiles = array();
		foreach ($user->profiles as $profile) {
			$profiles[$profile->service] = $profile;
		}

		$component = Yii::app()->getComponent('eauth');
		$services = $component->getServices();

		$model = new UserForm();
		$model->setAttributes($user->getAttributes());
		$auth_items = CHtml::listData(AuthItem::model()->findAll(),'name','description');
		$this->render('settings', array('model'=>$model, 'auth_items'=>$auth_items, 'profiles'=>$profiles, 'services'=>$services));
	}
}