<?php
class ImController extends ControllerCabinet {
	
	public $owner;
	public $responder;
	public function actionIndex() {
		$command = Yii::app()->db->createCommand();
		$command->from = 'vk_im t';
		$command->select = implode ( ', ', array(
				't.user_id',
				'(SELECT im.date FROM vk_im im WHERE im.user_id = t.user_id ORDER BY im.date DESC LIMIT 1) date',
				'(SELECT im.out FROM vk_im im WHERE im.user_id = t.user_id ORDER BY im.date DESC LIMIT 1) out_m',
				'SUM(CASE WHEN t.read_state = 0 THEN 1 ELSE 0 END) not_read',
				'(SELECT im.body FROM vk_im im WHERE im.user_id = t.user_id ORDER BY im.date DESC LIMIT 1) last_message',
		));
		$command->group = 't.user_id';
		$command->order = 'date DESC';
// 		$command->limit = 6;
// 		$dialogs = $command->queryAll(true);
// 		$dialogs = VkIm::model()->findAll($criteria);
// 		$count = $command->queryScalar();
		$count = Yii::app()->db->createCommand();
		$count->from = 'vk_im t';
		$count->select = implode ( ',', array(
				'COUNT(DISTINCT t.user_id)',
		));
// 		$count->group = 't.user_id';
		$total = $count->queryScalar();
		print_r($total);
		$dataProvider=new CSqlDataProvider($command->getText(), array(
				'totalItemCount'=>$total,
				'keyField'=>'user_id',
// 				'sort'=>array(
// 						'attributes'=>array(
// 								'date',
// 						),
// 				),
				'pagination'=>array(
						'pageSize'=>6,
				),
		));
// 		print_r($dataProvider->getData());
// 		$dataProvider->keyField = 't.user_id';
		
		$this->render('index', array('provider'=>$dataProvider));

	}

	public function actionDialog($id) {
		
		$criteria = new CDbCriteria();
		$criteria->condition = 't.user_id = :user_id';
		$criteria->order = 't.date';
		$criteria->params = array(':user_id'=>$id);
		$provider = new CActiveDataProvider('VkIm', array(
				'criteria'=>$criteria,
				'pagination' =>array(
						'pageSize'=>100,
				),
		));
		
		$helper = new CUrlHelper();
		$fields = array(
				'user_ids'=>$id . ',68651428',
				'fields'=>'photo_50,last_seen',
				'access_token'=>'c21989b89279a0424031a40df56ab242e4b48c92c016579ce072c30b4c036806f0475f0390ac903d780df',
				'name_case'=>'Nom',
				'v'=>'5.52'
		);
		$helper->execute('https://api.vk.com/method/users.get?'.http_build_query($fields), array());
		if($helper->isHttpOK()){
			$vk_data = CJSON::decode($helper->content);
		}
		$this->responder = $vk_data['response'][0]; 
		$this->owner = $vk_data['response'][1];
		
		$this->render('dialog', array('provider'=>$provider));
	}
}