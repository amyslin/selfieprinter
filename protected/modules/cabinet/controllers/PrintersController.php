<?php
class PrintersController extends ControllerCabinet {
	
	public function init() {
		parent::init();
		$this->title = 'Принтеры';
		$this->subtitle = '';
		array_push($this->broad, array('title'=>'Принтеры', 'url'=>array('/cabinet/printers')));
	}
	
	public function actionIndex() {
		$provider = new CActiveDataProvider('PtPrinter', array(
// 				'criteria'=>$criteria,
				'pagination' =>array(
						'pageSize'=>$this->perpage,
				)
		));
		$this->render('index',array('provider'=>$provider));
	}
	
	public function actionAdd() {
		array_push($this->broad, array('title'=>'Новый принтер', 'url'=>array('#')));
		$model = new PrinterForm();
		
		if (isset($_POST['PrinterForm'])&&isset($_POST['PrinterStorage'])) {
			$model->setAttributes($_POST['PrinterForm']);
			$model->setStorage($_POST['PrinterStorage']);
			$this->performAjaxValidation($model);
			$this->performAjaxValidation($model->storage);
			if ($model->validate()) {
 
				
				$printer = new PtPrinter();
				$printer->setAttributes($_POST['PrinterForm'], false);
				$printer->settings_storage = $model->storage->getAttributes(); 
				$printer->user_id = $this->user->user_id;

				if($printer->save(false)) {
					$this->redirect('index');
				}
			}
		}
		$this->render('add', array('model'=>$model));
		
	}
	
	public function actionUpdate($id) {
		array_push($this->broad, array('title'=>'Редактировать', 'url'=>array('#')));
		
		$printer = PtPrinter::model()->findByPk($id);
		if (empty($printer))
			throw new CHttpException(403, 'Принтер не найден.');
		
		$model = new PrinterForm();
		$model->setAttributes($printer->getAttributes());
		$model->setStorage($printer->settings_storage);
		
		if (isset($_POST['PrinterForm'])&&isset($_POST['PrinterStorage'])) {
			$model->setAttributes($_POST['PrinterForm']);
			$model->setStorage($_POST['PrinterStorage']);
			$this->performAjaxValidation($model);
			$this->performAjaxValidation($model->storage);
			if ($model->validate()) {
				$printer->setAttributes($_POST['PrinterForm'], false);
				$printer->settings_storage = $model->storage->getAttributes();
				$printer->user_id = $this->user->user_id;
			
				if($printer->save(false))
					Yii::app()->user->setFlash('success', 'Изменения успешно сохранены');
				
			}
		}
		
		$this->render('update', array('model'=>$model));
		
		
	}
	
	public function actionDelete($id) {
		
	}
}