<?php
class TemplatesController extends ControllerCabinet {

	public function init() {
		parent::init();
		$this->title = 'Шаблоны';
		$this->subtitle = '';
		array_push($this->broad, array('title'=>'Шаблоны', 'url'=>array('/cabinet/templates')));
	}

	public function actionIndex()
	{
		$criteria = new CDbCriteria();
		$criteria->order = 't.c_time DESC';

		$provider = new CActiveDataProvider('PtTemplate', array(
		// 				'criteria'=>$criteria,
				'pagination' =>array(
						'pageSize'=>$this->perpage,
				)
		));
		$this->render('index',array('provider'=>$provider));

	}

	public function actionAdd()
	{
		array_push($this->broad, array('title'=>'Новый принтер', 'url'=>array('#')));
		$model = new TemplateForm();
		if (isset($_POST['TemplateForm'])) {
			$model->setAttributes($_POST['TemplateForm']);
			$file = CUploadedFile::getInstance($model, 'data');

			$model->data = $file;
// 			$model->setStorage($_POST['PrinterStorage']);
// 			$this->performAjaxValidation($model);
// 			$this->performAjaxValidation($model->storage);
			if ($model->validate()) {

				$tmpFile = file_get_contents($file->getTempName());
				$template = new PtTemplate();
				$template->setAttributes($model->getAttributes(), false);
				$template->data = $tmpFile;
// 				$template->user_id = $this->user->user_id;

				if($template->save(false)) {
					$this->redirect('index');
				}
			}
		}
		$this->render('add', array('model'=>$model));

	}

	public function actionUpdate($id)
	{

	}
}