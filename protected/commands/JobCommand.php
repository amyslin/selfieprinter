<?php
class JobCommand extends CConsoleCommand {
	public function actionIndex() {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.job_status = "RUN"';
		$criteria->order = 't.c_time';

		$jobs = PtJob::model()->findAll($criteria);
		foreach ($jobs as $job) {
				$itemsCount = PtJobItem::model()->count(array('condition'=>'t.job_id = :job_id', 'params'=>array(':job_id'=>$job->job_id)));
				if (($job->time_limit>0) && (strtotime($job->c_time) + ($job->time_limit*60) > time())) {
					$job->job_status = "FINISHED";
					$job->save(false);
				}
				else if ($job->cnt_limit > 0 && ($itemsCount >= $job->cnt_limit)) {
					$job->job_status = "FINISHED";
					$job->save(false);
				}
				else {
					$className= ucfirst($job->type).'Service';
					if(class_exists($className)){
						$service=new $className();
						$service->update($job);
					}
				}
		}
	}

	public function actionPrepare() {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.job_status = "RUN"';
		$criteria->order = 't.c_time';
		$jobs = PtJob::model()->findAll($criteria);
		foreach ($jobs as $job) {
				$className= ucfirst($job->type).'Service';
				if(class_exists($className)){
					$service=new $className();
					$service->prepare($job);
				}
		}
	}

	public function actionPrintSubmit() {
		$criteria = new CDbCriteria();
		$criteria->condition = 'job.job_status = "RUN" AND t.request_id IS NULL AND t.print_file IS NOT NULL AND t.source_file IS NOT NULL';
		$criteria->order = 't.c_time';
		$criteria->with = array('job');

		$items = PtJobItem::model()->findAll($criteria);
		foreach ($items as $item) {
			$job = $item->job;
			$request = new PtRequest();
			$request->job_id = $job->job_id;
			$request->printer_id = $job->printer_id;
			if($request->save(false)) {
				$item->request_id = $request->request_id;
				$item->save(false);
			}


		}
	}
}