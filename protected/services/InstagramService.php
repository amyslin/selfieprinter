<?php
class InstagramService implements InterfaceService  {

	public function update($job) {
		$user = PtUser::model()->findByPk($job->user_id);
		$profile = PtProfile::model()->with(array('token'))->findByAttributes(array('user_id'=>$user->user_id, 'service'=>'instagram'));
// 		$token = $profile->token;
// 		$helper = new CInstaRequest();
// 		$helper->request('tags', trim(str_replace('#', '', $job->search_query)). '/media/recent', array(
// 				'access_token'=>$token->access_token,
// 				'count'=>10,
// 		));
		$helper = new CUrlHelper();
// 		$params = array(
// 				$job->search_query,
// 				'in',
// 				'100',
// 				'20',
// 				'1',
// 				'',
// 				'no',
// 				'0',
// 				'undefined',
// 				'no'
// 		);

		$unique = md5(uniqid());
		$query = $job->search_query;
		$helper->execute("https://www.instagram.com/explore/tags/$query/");
		// 		echo $helper->content;

		print_r("https://www.instagram.com/explore/tags/$query/");
// 		print_r($helper);
		if ($helper->isHttpOK()) {

// 			$res = preg_replace('/(<script[^>]*>.*?<\/script>)/is', "", $helper->content);
// 			$res = preg_replace('/(<noscript[^>]*>.*?<\/noscript>)/is', "", $res);
// 			$res = preg_replace('/(<style[^>]*>.*?<\/style>)/is', "", $res);
			// 		$res = mb_convert_encoding($res, 'utf-8', mb_detect_encoding($res));

// 			$xmlPath=new XmlPath($res, $html=true);
			preg_match('/<script type=\"text\/javascript\">window\._sharedData\s*=\s*(.*);<\//', $helper->content, $matches);
			//print_r($data);
			$data = CJSON::decode($matches[1]);
			
// 			print_r(CJSON::decode($matches[1]));
			if (isset($data['entry_data']['TagPage'][0]['tag']['media']['nodes'])) {
				$nodes = $data['entry_data']['TagPage'][0]['tag']['media']['nodes'];
				foreach ($nodes as $node) {
					if($node['is_video']==false){
						if (isset($node['code'])&&isset($node['id'])&&isset($node['owner']['id'])) {

							if(PtJobItem::model()->count(array(
									'condition'=>'job_id = :job_id AND source_id = :source_id',
									'params'=>array(
											':job_id'=>$job->job_id,
											':source_id'=>$node['id'],
									)
							))==0)
								$this->getMediaInfo("https://www.instagram.com/p/{$node['code']}/", $job);
						}

					}
				}
			}

		}

		else {
			echo 'request error';
		}

	}



	public function prepare($job) {
		$items = PtJobItem::model()->findAll(array(
			'condition'=>'t.job_id = :job_id AND t.print_file IS NULL',
			'params'=>array(':job_id'=>$job->job_id),
			'limit'=>40,
		));
		$template = $job->template;
		foreach($items as $item) {
			$userData = CJSON::decode($item->source_data);
			$params = array(
					'tag'=>array(
							'type'=>'text',
							'value'=>'#'.$item->job->search_query,
					),
					'image'=>array(
							'type'=>'image',
							'value'=>$item->source_file,
					),
			);
			if (isset($userData['userpic']))
				$params['userpic'] = array(
						'type'=>'image',
						'value'=>$userData['userpic']
				);

			if (isset($userData['username']))
				$params['username'] = array(
						'type'=>'text',
						'value'=>$userData['username']
				);
			$sourcefile = $this->getImagesPath($job) . DIRECTORY_SEPARATOR . $userData['source_id'] .'_ready.png';
			$printfile = $this->getImagesPath($job) . DIRECTORY_SEPARATOR . $userData['source_id'] .'_ready.pdf';
			file_put_contents($sourcefile, $template->renderTemplate($params));
			$imagick = new Imagick();
			$imagick->readImageBlob(file_get_contents($sourcefile));
			// 		$imagick->setresolution(300, 300);
			// 		$imagick->resampleImage(150,150,imagick::FILTER_UNDEFINED,1);
			$imagick->setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
			$imagick->setimageformat('pdf');
			file_put_contents($printfile, $imagick->getImageBlob());
			$item->print_file = $printfile;
			$item->save(false);
		}

	}

	protected function getImagesPath($job) {
		$path=Yii::app()->getRuntimePath() . DIRECTORY_SEPARATOR . 'jobs' . DIRECTORY_SEPARATOR . date('Y-m-d') . '_' .$job->job_id;
// 		echo $path;
		if(!file_exists($path))
			mkdir($path, 0777);
		return $path;
	}

	private function getMediaInfo($url, $job) {

		$helper = new CUrlHelper();
		$helper->execute($url);

// 		$res = preg_replace('/(<script[^>]*>.*?<\/script>)/is', "", $helper->content);
// 		$res = preg_replace('/(<noscript[^>]*>.*?<\/noscript>)/is', "", $res);
// 		$res = preg_replace('/(<style[^>]*>.*?<\/style>)/is', "", $res);
		// 		$res = mb_convert_encoding($res, 'utf-8', mb_detect_encoding($res));
// 		print_r($res);

		if($helper->isHttpOK()) {

			preg_match('/<script type=\"text\/javascript\">window\._sharedData\s*=\s*(.*);<\//', $helper->content, $matches);
			if(count($matches)<2)
				return null;
			$data = CJSON::decode($matches[1]);
			
			//print_r($matches[1]);

			$imageInfo =  array(
					'source_img'=>(isset($data['entry_data']['PostPage'][0]['graphql']['shortcode_media']['display_url']))?$data['entry_data']['PostPage'][0]['graphql']['shortcode_media']['display_url']:null,
					'userpic'=>(isset($data['entry_data']['PostPage'][0]['graphql']['shortcode_media']['owner']['profile_pic_url']))?$data['entry_data']['PostPage'][0]['graphql']['shortcode_media']['owner']['profile_pic_url']:null,
					'username'=>(isset($data['entry_data']['PostPage'][0]['graphql']['shortcode_media']['owner']['username']))?$data['entry_data']['PostPage'][0]['graphql']['shortcode_media']['owner']['username']:null,
					'userlname'=>(isset($data['entry_data']['PostPage'][0]['graphql']['shortcode_media']['owner']['full_name']))?$data['entry_data']['PostPage'][0]['graphql']['shortcode_media']['owner']['full_name']:null,
					'user_id'=>(isset($data['entry_data']['PostPage'][0]['graphql']['shortcode_media']['owner']['id']))?$data['entry_data']['PostPage'][0]['graphql']['shortcode_media']['owner']['id']:null,
					'source_id'=>(isset($data['entry_data']['PostPage'][0]['graphql']['shortcode_media']['id']))?$data['entry_data']['PostPage'][0]['graphql']['shortcode_media']['id']:null,
					'time'=>(isset($data['entry_data']['PostPage'][0]['graphql']['shortcode_media']['taken_at_timestamp']))?$data['entry_data']['PostPage'][0]['graphql']['shortcode_media']['taken_at_timestamp']:null,
					'likes'=>(isset($data['entry_data']['PostPage'][0]['graphql']['shortcode_media']['edge_media_preview_like']['count']))?$data['entry_data']['PostPage'][0]['graphql']['shortcode_media']['edge_media_preview_like']['count']:null,
			);

			//print_r($imageInfo);

			if (!empty($imageInfo['source_img'])){
				$count = PtJobItem::model()->count(array('condition'=>'t.source_id = :sid AND t.source_userid = :uid', 'params'=>array(
						':sid'=>$imageInfo['source_id'],
						':uid'=>$imageInfo['user_id'],
				)));
				if ($count)
					continue;
				$item = new PtJobItem();
				$item->job_id = $job->job_id;
				$item->source_id = $imageInfo['source_id'];
				$item->source_userid = $imageInfo['user_id'];

				$item->source_data = CJSON::encode($imageInfo);
				if (isset($imageInfo['source_img'])) {
					$content = file_get_contents($imageInfo['source_img']);

					$ext = '.jpg';
					if(preg_match('/.*?(\.[a-zA-Z]{3,5})$/s', $imageInfo['source_img'], $matches))
						$ext = $matches[1];


					$sourcefile = $this->getImagesPath($job) . DIRECTORY_SEPARATOR . $imageInfo['source_id'] .'_scr' . $ext;
					file_put_contents($sourcefile, $content);
					$item->source_file = $sourcefile;


				}
				$item->save(false);
			}

		}
		else
			return null;

	}
}