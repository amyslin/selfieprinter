<?php
class InstaService implements InterfaceService  {

	public function update($job) {
		$user = PtUser::model()->findByPk($job->user_id);
		$profile = PtProfile::model()->with(array('token'))->findByAttributes(array('user_id'=>$user->user_id, 'service'=>'instagram'));
// 		$token = $profile->token;
// 		$helper = new CInstaRequest();
// 		$helper->request('tags', trim(str_replace('#', '', $job->search_query)). '/media/recent', array(
// 				'access_token'=>$token->access_token,
// 				'count'=>10,
// 		));
		$helper = new CUrlHelper();
// 		$params = array(
// 				$job->search_query,
// 				'in',
// 				'100',
// 				'20',
// 				'1',
// 				'',
// 				'no',
// 				'0',
// 				'undefined',
// 				'no'
// 		);

		$unique = md5(uniqid());
		$query = $job->search_query;
		$helper->execute("https://www.instagram.com/explore/tags/$query/");
		// 		echo $helper->content;

		print_r("https://www.instagram.com/explore/tags/$query/");
// 		print_r($helper);
		if ($helper->isHttpOK()) {

// 			$res = preg_replace('/(<script[^>]*>.*?<\/script>)/is', "", $helper->content);
// 			$res = preg_replace('/(<noscript[^>]*>.*?<\/noscript>)/is', "", $res);
// 			$res = preg_replace('/(<style[^>]*>.*?<\/style>)/is', "", $res);
			// 		$res = mb_convert_encoding($res, 'utf-8', mb_detect_encoding($res));

// 			$xmlPath=new XmlPath($res, $html=true);
			preg_match('/<script type=\"text\/javascript\">window\._sharedData\s*=\s*(.*);<\//', $helper->content, $matches);
			$data = CJSON::decode($matches[1]);
// 			print_r(CJSON::decode($matches[1]));
			if (isset($data['entry_data']['TagPage'][0]['tag']['media']['nodes'])) {
				$nodes = $data['entry_data']['TagPage'][0]['tag']['media']['nodes'];
				foreach ($nodes as $node) {
					if($node['is_video']==false){
						if (isset($node['code'])) {
							$helper = new CUrlHelper();
							$helper->execute("https://www.instagram.com/p/{$node['code']}/");
							print_r($helper);
						}

					}
				}
			}

		}

		else {
			echo 'request error';
		}

	}



	public function prepare($job) {
		$items = PtJobItem::model()->findAll(array(
			'condition'=>'t.job_id = :job_id AND t.print_file IS NULL',
			'params'=>array(':job_id'=>$job->job_id),
			'limit'=>40,
		));
		$template = $job->template;
		foreach($items as $item) {
			$userData = CJSON::decode($item->source_data);
			$params = array(
					'tag'=>array(
							'type'=>'text',
							'value'=>'#'.$item->job->search_query,
					),
					'image'=>array(
							'type'=>'image',
							'value'=>$item->source_file,
					),
			);
			if (isset($userData['userpic']))
				$params['userpic'] = array(
						'type'=>'image',
						'value'=>$userData['userpic']
				);

			if (isset($userData['username']))
				$params['username'] = array(
						'type'=>'text',
						'value'=>$userData['username']
				);
			$sourcefile = $this->getImagesPath($job) . DIRECTORY_SEPARATOR . $userData['source_id'] .'_ready.png';
			$printfile = $this->getImagesPath($job) . DIRECTORY_SEPARATOR . $userData['source_id'] .'_ready.pdf';
			file_put_contents($sourcefile, $template->renderTemplate($params));
			$imagick = new Imagick();
			$imagick->readImageBlob(file_get_contents($sourcefile));
			// 		$imagick->setresolution(300, 300);
			// 		$imagick->resampleImage(150,150,imagick::FILTER_UNDEFINED,1);
			$imagick->setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
			$imagick->setimageformat('pdf');
			file_put_contents($printfile, $imagick->getImageBlob());
			$item->print_file = $printfile;
			$item->save(false);
		}

	}

	protected function getImagesPath($job) {
		$path=Yii::app()->getRuntimePath() . DIRECTORY_SEPARATOR . 'jobs' . DIRECTORY_SEPARATOR . date('Y-m-d') . '_' .$job->job_id;
// 		echo $path;
		if(!file_exists($path))
			mkdir($path, 0777);
		return $path;
	}

	private function getMediaInfo($url, $job) {

		preg_match('/\/([\d_]+)\//', $url, $matches);

		if (!isset($matches[1]))
			return null;

		if(PtJobItem::model()->count(array(
			'condition'=>'job_id = :job_id AND source_id = :source_id',
			'params'=>array(
				':job_id'=>$job->job_id,
				':source_id'=>$matches[1],
			)
		))>0)
			return null;

		$helper = new CUrlHelper();
		$helper->execute($url);

		$res = preg_replace('/(<script[^>]*>.*?<\/script>)/is', "", $helper->content);
		$res = preg_replace('/(<noscript[^>]*>.*?<\/noscript>)/is', "", $res);
		$res = preg_replace('/(<style[^>]*>.*?<\/style>)/is', "", $res);
		// 		$res = mb_convert_encoding($res, 'utf-8', mb_detect_encoding($res));
		print_r($res);
		if($helper->isHttpOK()) {
			$xmlPath=new XmlPath($res, $html=true);

			$result =  $xmlPath->queryAll(array(
					'source_img'=>'//div[@class="main-photo-big"][1]//img[1]/@src',
					'userpic'=>'//div[@class="profilephoto"][1]//img[1]/@src',
					'username'=>'//span[@class="blueusername"][1]/text()',
					'userlname'=>'//div[@class="username fl"][1]//span[@class="date"][1]/text()',
			), null, true);
			// 			if (!count($result))
			// 				return null;
			if(count($matches)==2) {
				$extra = explode('_', $matches[1]);
				$result['source_id'] = $matches[1];
				$result['user_id'] = $extra[1];
			}
			return $result;

		}
		else
			return null;

	}
}