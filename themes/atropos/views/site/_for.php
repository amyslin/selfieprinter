<section class="container text-center padding100" id="for">
				<h1 class="text-center">
					<strong>Для чего?</strong><br/>
					<span class="subtitle">СОБЫТИЙНЫЙ МАРКЕТИНГ И ПРОДВИЖЕНИЕ В СОЦИАЛЬНЫХ СЕТЯХ</span>
				</h1>
				<p class="lead">Селфипринтер - это отличный интерактивный маркетинговый инструмент, который делает персонализированные брендированные сувениры и генерирует контент в социальных сетях</p>
</section>


<div class="container">
			<hr class="half-margins" /><!-- hr line -->
</div>

<section class="container">
<div class="row">
	<div class="col-md-4">
		<div class="featured-box nobg border-only">
				<i class="fa fa-gift"></i>
				<h4>Приятный сувенир</h4>
				<p>Посетителю остается на память фото с вашим логотипом, и он оставит его себе!<br/>Гарантированно, что фото не окажется в ближайшей корзине!</p>
		</div>
	</div>
	<div class="col-md-4">
		<div class="featured-box nobg border-only left-separator">
				<i class="fa fa-tags"></i>
				<h4>Брендирование</h4>
				<p>Для каждого мероприятия может быть разработан свой уникальный дизайн шаблона, на котором можно разместить логотип и другую сопровождающую информацию</p>
		</div>
	</div>
	<div class="col-md-4">
		<div class="featured-box nobg border-only left-separator">
				<i class="fa fa-users"></i>
				<h4>SMM маркетинг</h4>
				<p>Пользователи генерируют меда-контент.<br/> Сгенерированный пользователем контент вызывает больше доверия у его подписчиков и друзей, таким образом вы охватываете еще и подписчиков!</p>
		</div>
	</div>

</div>
<div class="row">
	<div class="col-md-4">
		<div class="featured-box nobg border-only">
				<i class="fa fa-smile-o"></i>
				<h4>Эффективность</h4>
				<p>Это интересный и не навязчивый способ промоутинга. Ваши промо-акции будут привлекать больше внимания и работать эффективнее.</p>
		</div>
	</div>
	<div class="col-md-4">
		<div class="featured-box nobg border-only left-separator">
				<i class="fa fa-thumbs-up"></i>
				<h4>Простота</h4>
				<p>Почти каждый сейчас имеет под рукой смартфон с приложениями социальных сетей и активно ими пользуется, для большинства не составит труда разместить фото с <b>#хэштэгом</b></p>
		</div>
	</div>
	<div class="col-md-4">
		<div class="featured-box nobg border-only left-separator">
				<i class="fa fa-bar-chart-o"></i>
				<h4>Аналитика</h4>
				<p>Вы можете легко отследить результат, наша система собирает аналитику и генерирует отчеты по каждому мероприятию в автоматическом режиме.</p>
		</div>
	</div>

</div>
</section>

<div class="container">
			<hr class="half-margins"><!-- hr line -->
</div>