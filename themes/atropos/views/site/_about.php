<section class="container text-center" id="about">
				<h1 class="text-center">
					<strong>О селфипринтере</strong><br/>
					<span class="subtitle">МОМЕНТАЛЬНАЯ ПЕЧАТЬ ФОТО ИЗ СОЦИАЛЬНЫХ СЕТЕЙ</span>
				</h1>
				<p class="lead">Селфипринтер - это терминал моментальной печати фото из социальных сетей.<br/> Печатает фотографии ваших гостей по уникальному хэштэгу или сочетанию хэштэгов в режиме реального времени, это невероятно просто и не требует особых навыков</p>
</section>


<div class="container">
			<hr class="half-margins" /><!-- hr line -->
</div>

<section class="container">
<div class="row">
	<div class="col-md-4">
		<div class="featured-box nobg border-only">
				<i class="fa fa-instagram"></i>
				<h4>Instagram</h4>
				<p>Печать фото из Instagram по хэштэгу</p>
		</div>
	</div>
	<div class="col-md-4">
		<div class="featured-box nobg border-only left-separator">
				<i class="fa fa-vk"></i>
				<h4>Вконтакте</h4>
				<p>Печать фото из Вконтакте по хэштэгу или со стены вашей группы</p>
		</div>
	</div>
	<div class="col-md-4">
		<div class="featured-box nobg border-only left-separator">
				<i class="fa fa-facebook"></i>
				<h4>Facebook</h4>
				<p>Печать фото из Facebook по хэштэгу или со стены вашей группы</p>
		</div>
	</div>

</div>
</section>