<section class="special-row padding100" id="how">
<div class="container">
	<h1 class="text-center"><strong>Как работает</strong> Селфипринтер</h1>
	<div class="divider">
	<i class="fa fa-camera-retro"></i>
</div>
<div class="row">
	<div class="col-md-4">
		<h1 class="text-center"><strong>1</strong></h1>
		<i class="featured-icon pull-left fa fa-mobile"/></i>
		<p>Ваш посетитель размещает фотографию в социальной сети (<b>Instagram</b>, <b>Вконтакте</b>, <b>Facebook</b>) используя мобильное устройство, Помечает ее уникальным <b>#хэштэгом</b></p>
	</div>
	<div class="col-md-4">
		<h1 class="text-center"><strong>2</strong></h1>
		<i class="featured-icon pull-left fa fa-cloud"/></i>
		<p style="padding-top: 20px;">Наш облачный сервис в постоянном режиме мониторит указанный <b>#хэштэг</b> и отправляет новые фотографии на печать</p>
	</div>
	<div class="col-md-4">
		<h1 class="text-center"><strong>3</strong></h1>
		<i class="featured-icon pull-left fa fa-file-image-o"/></i>
		<p style="padding-top: 10px;">Отправленная на печать фотография печатается не более чем через 15 секунд после публикации, посетитель заберает фото на стойке с Селфипринтером</p>
	</div>

</div>
</div>


</section>