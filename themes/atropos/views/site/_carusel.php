<style>
.tp-bgimg {
	background-color: #FFF !important;
}
</style>
<div class="fullwidthbanner-container roundedcorners">
			<div class="fullwidthbanner">
					<ul>

						<!-- SLIDE  -->
						<li data-transition="fade" data-slotamount="7" data-masterspeed="1000">

							<!-- COVER IMAGE -->
							<img src="/themes/atropos/selfieprinter.jpg" alt="" data-bgfit="contain" data-bgposition="right top" data-bgrepeat="no-repeat" />

							<div class="tp-caption modern_big_redbg"
								data-x="80"
								data-y="66"
								data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
								data-speed="500"
								data-start="800"
								data-easing="Back.easeOut"
								data-endspeed="500"
								data-endeasing="Power4.easeIn"
								data-captionhidden="off"
								style="z-index: 4">#Селфипринтер
							</div>

							<div class="tp-caption thintext_dark"
								data-x="80"
								data-y="120"
								data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
								data-speed="500"
								data-start="800"
								data-easing="Back.easeOut"
								data-endspeed="500"
								data-endeasing="Power4.easeIn"
								data-captionhidden="off"
								style="z-index: 4">
									Терминал моментальной
									печати фотографий <br/>из социальных сетей по <b>#хэштэгу</b>
							</div>

							<div class="tp-caption thinheadline_dark"
								data-x="80"
								data-y="180"
								data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
								data-speed="500"
								data-start="800"
								data-easing="Back.easeOut"
								data-endspeed="500"
								data-endeasing="Power4.easeIn"
								data-captionhidden="off"
								style="z-index: 4">
									Раскрасит любое мероприятие
							</div>

							<div class="tp-caption medium_grey"
								data-x="80"
								data-y="230"
								data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
								data-speed="500"
								data-start="1500"
								data-easing="Back.easeOut"
								data-endspeed="500"
								data-endeasing="Power4.easeIn"
								data-captionhidden="off"
								style="z-index: 4">
									#Концерт
							</div>
							<div class="tp-caption medium_grey"
								data-x="190"
								data-y="230"
								data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
								data-speed="500"
								data-start="2000"
								data-easing="Back.easeOut"
								data-endspeed="500"
								data-endeasing="Power4.easeIn"
								data-captionhidden="off"
								style="z-index: 4">
									#Презентация
							</div>
							<div class="tp-caption medium_grey"
								data-x="345"
								data-y="230"
								data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
								data-speed="500"
								data-start="2500"
								data-easing="Back.easeOut"
								data-endspeed="500"
								data-endeasing="Power4.easeIn"
								data-captionhidden="off"
								style="z-index: 4">
									#Вечеринка
							</div>
							<div class="tp-caption medium_grey"
								data-x="80"
								data-y="260"
								data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
								data-speed="500"
								data-start="3000"
								data-easing="Back.easeOut"
								data-endspeed="500"
								data-endeasing="Power4.easeIn"
								data-captionhidden="off"
								style="z-index: 4">
									#Ресторан
							</div>
							<div class="tp-caption medium_grey"
								data-x="200"
								data-y="260"
								data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
								data-speed="500"
								data-start="3500"
								data-easing="Back.easeOut"
								data-endspeed="500"
								data-endeasing="Power4.easeIn"
								data-captionhidden="off"
								style="z-index: 4">
									#Клуб
							</div>
							<div class="tp-caption medium_grey"
								data-x="275"
								data-y="260"
								data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
								data-speed="500"
								data-start="4000"
								data-easing="Back.easeOut"
								data-endspeed="500"
								data-endeasing="Power4.easeIn"
								data-captionhidden="off"
								style="z-index: 4">
									#Праздник
							</div>
							<div class="tp-caption medium_grey"
								data-x="80"
								data-y="290"
								data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
								data-speed="500"
								data-start="4500"
								data-easing="Back.easeOut"
								data-endspeed="500"
								data-endeasing="Power4.easeIn"
								data-captionhidden="off"
								style="z-index: 4">
									#Промо-акция
							</div>

							<div class="tp-caption"
								data-x="80"
								data-y="320"
								data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
								data-speed="500"
								data-start="5000"
								data-easing="Back.easeOut"
								data-endspeed="500"
								data-endeasing="Power4.easeIn"
								data-captionhidden="off"
								style="z-index: 4"
							>
								<i class="featured-icon fa fa-instagram"></i> <i class="featured-icon fa fa-vk"></i> <i class="featured-icon fa fa-facebook"></i>
							</div>

						</li>
						<!-- SLIDE  -->
						<li data-transition="3dcurtain-vertical" data-slotamount="14" data-masterspeed="1000">

							<!-- COVER IMAGE -->
							<img src="/themes/atropos/JHwSYMoUAsc.jpg" alt="" data-bgfit="cover" data-bgposition="right center" data-bgrepeat="no-repeat" />

							<div class="tp-caption modern_big_redbg"
								data-x="80"
								data-y="66"
								data-customin="x:-400;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:0% 0%;"
								data-customout="x:500;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:0% 0%;"


								data-speed="500"
								data-start="1000"
								data-easing="Power3.easeIn"

								data-splitin="none"
								data-elementdelay="1.5"

								data-endspeed="500"
								data-endeasing="Power1.easeInOut"

								data-splitout="none"
								data-endelementdelay="0.1"
								style="z-index: 4">#Концерты
							</div>


						</li>
						<!-- SLIDE  -->
						<li data-transition="incube-horizontal" data-slotamount="5" data-masterspeed="1000">

							<!-- COVER IMAGE -->
							<img src="/themes/atropos/fa7de7IkxnQ.jpg" alt="" data-bgfit="cover" data-bgposition="right center" data-bgrepeat="no-repeat" />

							<div class="tp-caption mediumwhitebg"
								data-x="80"
								data-y="66"
								data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
								data-speed="500"
								data-start="1000"
								data-easing="Back.easeOut"
								data-endspeed="500"
								data-endeasing="Power4.easeIn"
								data-captionhidden="off"
								style="z-index: 4">#Презентации
							</div>


						</li>
						<li data-transition="fade" data-masterspeed="1000">

							<!-- COVER IMAGE -->
							<img src="/themes/atropos/vEk9aSa6Fdk.jpg" alt="" data-bgfit="cover" data-bgposition="right center" data-bgrepeat="no-repeat" />


							<div class="tp-caption modern_big_redbg"
								data-x="80"
								data-y="66"
								data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
								data-speed="500"
								data-start="800"
								data-easing="Back.easeOut"
								data-endspeed="500"
								data-endeasing="Power4.easeIn"
								data-captionhidden="off"
								style="z-index: 4">#Вечеринки
							</div>

						</li>

					</ul>
					<div class="tp-bannertimer"></div>
				</div>

</div>