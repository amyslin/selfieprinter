<section class="special-row padding100" id="order">
<div class="container" >
	<h1 class="text-center" ><strong>Заказать</strong> Селфипринтер</h1>
	<div class="divider">
	<i class="fa fa-envelope"></i>
	</div>
	<div class="row">
		<div class="col-md-12">
		<form id="contactForm" class="white-row" action="/#order" method="post">
							<div class="row">
								<div class="form-group">
									<div class="col-md-4">
										<label>Полное имя *</label>
										<input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="contact_name" id="contact_name">
									</div>
									<div class="col-md-4">
										<label>Email*</label>
										<input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="contact_email" id="contact_email">
									</div>
									<div class="col-md-4">
										<label>Телефон</label>
										<input type="text" value="" data-msg-required="Please enter your phone" data-msg-email="Please enter your phone" maxlength="100" class="form-control" name="contact_phone" id="contact_phone">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
									<div class="col-md-12">
										<label>Сообщение *</label>
										<textarea maxlength="5000" data-msg-required="Please enter your message." rows="10" class="form-control" name="contact_message" id="contact_message"></textarea>
									</div>
								</div>
							</div>

							<br />

							<div class="row">
								<div class="col-md-12">
									<span class="pull-right"><!-- captcha -->
										<?php 
										$this->widget('CCaptcha', [
											'buttonLabel' => '<span class="fa fa-refresh"></span> обновить картинку',
											'buttonOptions' => ['style' => 'display: block;']
										]);
										?>
									</span>

									<input id="contact_submit" type="submit" value="Send Message" class="btn btn-primary btn-lg" data-loading-text="Loading...">
								</div>
							</div>
						</form>
				</div>

	</div>

	<h1 class="text-center" ><strong>Телефон: </strong>+7-992-214-02-53, +7-952-651-27-09</h1>

</div>
</section>