<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   width="240"
   height="55"
   id="svg2"
   version="1.1"
   inkscape:version="0.48.5 r10040"
   sodipodi:docname="logo.svg">
  <defs
     id="defs4">
    <linearGradient
       id="FontGradient"
       x1="0"
       y1="0"
       x2="100%"
       y2="0%">
      <!--20% spreadMethod="repeat"-->
      <stop
         offset="0%"
         stop-color="crimson"
         id="stop3042" />
      <stop
         offset="10%"
         stop-color="purple"
         id="stop3044" />
      <stop
         offset="10%"
         stop-color="red"
         id="stop3046" />
      <stop
         offset="20%"
         stop-color="crimson"
         id="stop3048" />
      <stop
         offset="20%"
         stop-color="orangered"
         id="stop3050" />
      <stop
         offset="30%"
         stop-color="red"
         id="stop3052" />
      <stop
         offset="30%"
         stop-color="orange"
         id="stop3054" />
      <stop
         offset="40%"
         stop-color="orangered"
         id="stop3056" />
      <stop
         offset="40%"
         stop-color="gold"
         id="stop3058" />
      <stop
         offset="50%"
         stop-color="orange"
         id="stop3060" />
      <stop
         offset="50%"
         stop-color="yellowgreen"
         id="stop3062" />
      <stop
         offset="60%"
         stop-color="gold"
         id="stop3064" />
      <stop
         offset="60%"
         stop-color="green"
         id="stop3066" />
      <stop
         offset="70%"
         stop-color="yellowgreen"
         id="stop3068" />
      <stop
         offset="70%"
         stop-color="steelblue"
         id="stop3070" />
      <stop
         offset="80%"
         stop-color="skyblue"
         id="stop3072" />
      <stop
         offset="80%"
         stop-color="mediumpurple"
         id="stop3074" />
      <stop
         offset="90%"
         stop-color="steelblue"
         id="stop3076" />
      <stop
         offset="90%"
         stop-color="purple"
         id="stop3078" />
      <stop
         offset="100%"
         stop-color="mediumpurple"
         id="stop3080" />
    </linearGradient>
    <linearGradient
       id="FontGradient2"
       x1="0"
       y1="0"
       x2="100%"
       y2="0%">
      <!--20% spreadMethod="repeat"-->
      <stop
         offset="0%"
         stop-color="red"
         id="stop3046" />
      <stop
         offset="20%"
         stop-color="crimson"
         id="stop3048" />
      <stop
         offset="20%"
         stop-color="gold"
         id="stop3058" />
      <stop
         offset="40%"
         stop-color="orange"
         id="stop3060" />
      <stop
         offset="40%"
         stop-color="green"
         id="stop3066" />
      <stop
         offset="60%"
         stop-color="yellowgreen"
         id="stop3068" />
      <stop
         offset="60%"
         stop-color="steelblue"
         id="stop3070" />
      <stop
         offset="80%"
         stop-color="skyblue"
         id="stop3072" />
      <stop
         offset="80%"
         stop-color="purple"
         id="stop3078" />
      <stop
         offset="100%"
         stop-color="mediumpurple"
         id="stop3080" />
    </linearGradient>
    <pattern
       id="p-stripes"
       patternUnits="userSpaceOnUse"
       width="200"
       height="200"
       viewBox="0 0 200 200">
      <rect
         width="200"
         height="200"
         fill="url(#FontGradient)"
         id="rect3083" />
    </pattern>
    <style
       type="text/css"
       id="style2996">/* cyrillic */
@font-face {
  font-family: 'Lobster';
  font-style: normal;
  font-weight: 400;
  src: local('Lobster'), local('Lobster-Regular'), url(https://fonts.gstatic.com/s/lobster/v15/c28rH3kclCLEuIsGhOg7evY6323mHUZFJMgTvxaG2iE.woff2) format('woff2');
  unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Lobster';
  font-style: normal;
  font-weight: 400;
  src: local('Lobster'), local('Lobster-Regular'), url(https://fonts.gstatic.com/s/lobster/v15/RdfS2KomDWXvet4_dZQehvY6323mHUZFJMgTvxaG2iE.woff2) format('woff2');
  unicode-range: U+0102-0103, U+1EA0-1EF1, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Lobster';
  font-style: normal;
  font-weight: 400;
  src: local('Lobster'), local('Lobster-Regular'), url(https://fonts.gstatic.com/s/lobster/v15/9NqNYV_LP7zlAF8jHr7f1vY6323mHUZFJMgTvxaG2iE.woff2) format('woff2');
  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Lobster';
  font-style: normal;
  font-weight: 400;
  src: local('Lobster'), local('Lobster-Regular'), url(https://fonts.gstatic.com/s/lobster/v15/cycBf3mfbGkh66G5NhszPQ.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}

/* latin */
@font-face {
  font-family: 'Pacifico';
  font-style: normal;
  font-weight: 400;
  src: local('Pacifico Regular'), local('Pacifico-Regular'), url(https://fonts.gstatic.com/s/pacifico/v7/Q_Z9mv4hySLTMoMjnk_rCfesZW2xOQ-xsNqO47m55DA.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}</style>
    <filter
       style="color-interpolation-filters:sRGB;"
       inkscape:label="Drop Shadow"
       id="filter3249">
      <feFlood
         flood-opacity="0.3"
         flood-color="rgb(0,0,0)"
         result="flood"
         id="feFlood3251" />
      <feComposite
         in="flood"
         in2="SourceGraphic"
         operator="in"
         result="composite1"
         id="feComposite3253" />
      <feGaussianBlur
         in="composite"
         stdDeviation="1"
         result="blur"
         id="feGaussianBlur3255" />
      <feOffset
         dx="2"
         dy="2"
         result="offset"
         id="feOffset3257" />
      <feComposite
         in="SourceGraphic"
         in2="offset"
         operator="over"
         result="composite2"
         id="feComposite3259" />
    </filter>
    <filter
       style="color-interpolation-filters:sRGB;"
       inkscape:label="Drop Shadow"
       id="filter3261">
      <feFlood
         flood-opacity="0.3"
         flood-color="rgb(0,0,0)"
         result="flood"
         id="feFlood3263" />
      <feComposite
         in="flood"
         in2="SourceGraphic"
         operator="in"
         result="composite1"
         id="feComposite3265" />
      <feGaussianBlur
         in="composite"
         stdDeviation="1"
         result="blur"
         id="feGaussianBlur3267" />
      <feOffset
         dx="2"
         dy="2"
         result="offset"
         id="feOffset3269" />
      <feComposite
         in="SourceGraphic"
         in2="offset"
         operator="over"
         result="composite2"
         id="feComposite3271" />
    </filter>
  </defs>
  <sodipodi:namedview
     id="base"
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1.0"
     inkscape:pageopacity="0.0"
     inkscape:pageshadow="2"
     inkscape:zoom="2.8"
     inkscape:cx="329"
     inkscape:cy="839"
     inkscape:document-units="px"
     inkscape:current-layer="layer1"
     showgrid="false"
     inkscape:window-width="1920"
     inkscape:window-height="1018"
     inkscape:window-x="-8"
     inkscape:window-y="-8"
     inkscape:window-maximized="1" />
  <metadata
     id="metadata7">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title />
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <g
     inkscape:label="Layer 1"
     inkscape:groupmode="layer"
     id="layer1">
    <text
       xml:space="preserve"
       style="font-size:40px;font-style:normal;font-weight:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:url(#FontGradient2);fill-opacity:1;stroke:none;font-family:Pacifico;filter:url(#filter3261)"
       x="0"
       y="40"
       id="text3003"
       sodipodi:linespacing="125%"><tspan
         sodipodi:role="line"
         id="tspan3005"
         x="0"
         y="40">SelfiePrinter</tspan></text>
  </g>
</svg>
