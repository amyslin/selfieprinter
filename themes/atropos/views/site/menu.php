<div class="navbar-collapse nav-main-collapse collapse pull-right">
					<nav class="nav-main mega-menu">
						<ul class="nav nav-pills nav-main scroll-menu" id="topMain">
<?php foreach ($items as $key=>$item):?>
	<li class="<?=($key==0)?'active':''?>"><a href="<?=$item['link']?>"><?=$item['title']?></a></li>
<?php endforeach;?>
						</ul>
					</nav>
				</div>
<?php 
$coreScript = Yii::app()->clientScript;
$coreScript->registerPackage('nav');
$script = <<<SCRIPT
			jQuery('#topMain').onePageNav({
				currentClass: 'active',
				changeHash: false,
				scrollSpeed: 750,
				scrollThreshold: 0.5,
				filter: ':not(.external)',
				easing: 'easeInOutExpo'
			});
SCRIPT;

$coreScript->registerScript('nav', $script, CClientScript::POS_END);

?>