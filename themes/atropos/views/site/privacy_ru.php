<header id="page-title"> <!-- style="background-image:url('assets/images/demo/parallax_bg.jpg')" -->
				<!--
					Enable only if bright background image used
					<span class="overlay"></span>
				-->

				<div class="container">
					<h1>Privacy Policy</h1>

					<ul class="breadcrumb">
						<li><a href="/">Home</a></li>
						<li class="active">Privacy Policy</li>
					</ul>
				</div>
			</header>
<section class="container">
	<p class="lead">Effective date: December 14, 2015</p>
	<ul>
		<li>This privacy policy governs your use of the software application SelfiePrinter (“Application”) for mobile devices that was created by LLC «BountyCards». The Application helps users to share photos on printer from social networks.</li>
		<li>By using our application you can allow printer to access pictures from social networks and then to print them on the device.</li>
		<li>Our Policy applies to all visitors, users, and others who access the Application ("Users").</li>
	</ul>
	
	<hr class="half-margins" /><!-- separator -->
	
	<h2>Types of Data collected</h2>
	<p>We collect the following types of information.</p>
	<p>Information you provide us directly:</p>
	<ul>
		<li>Username and email on linked social networks</li>
		<li>User Content (e.g., photos, video) that you post to the Service</li>
	</ul>
	<p>The Personal Data may be freely provided by the User, or collected automatically when using this Application.</p>
	<p>Any use of Cookies - or of other tracking tools - by this Application or by the owners of third party services used by this Application, unless stated otherwise, serves to identify Users and remember their preferences, for the sole purpose of providing the service required by the User.</p>
	<p>Users are responsible for any Personal Data of third parties obtained, published or shared through this Application and confirm that they have the third party's consent to provide the Data to the Owner.</p>
	<p>In addition, the Application may collect certain information automatically, including, but not limited to the IP address of your mobile device, geolocated data, your mobile operating system, the type of mobile Internet browsers you use, and information about the way you use the Application.</p>
	<p>When you visit the mobile application, we may use GPS technology (or other similar technology) to determine your current location in order to determine the city you are located within and display a location map with relevant advertisements. We will not share your current location with other users or partners.</p>
	<p>If you do not want us to use your location for the purposes set forth above, you should turn off the location services for the mobile application located in your account settings or in your mobile phone settings and/or within the mobile application.</p>
	
	<hr class="half-margins" /><!-- separator -->
	
	<h2>Sharing your information</h2>
	<p>We will not rent or sell your information to third parties outside without your consent, except as noted in this Policy.</p>
	
	<hr class="half-margins" /><!-- separator -->
	
	<h2>Parties with whom we may share your information:</h2>
	<ul>
		<li>Telecommunication services</li>
		<li>Partners of the Service, who helps finish the customer order (e.g. printing), issues through the Selfieprinter partner’s terminals.</li>
	</ul>
	<p>We may disclose User Provided and Automatically Collected Information:</p>
	<ul>
		<li>as required by law, such as to comply with a subpoena, or similar legal process;</li>
		<li>when we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to a government request;</li>
		<li>with our trusted services providers who work on our behalf, do not have an independent use of the information we disclose to them, and have agreed to adhere to the rules set forth in this privacy statement.</li>
		<li>if Selfieprinter is involved in a merger, acquisition, or sale of all or a portion of its assets, you will be notified via email and/or a prominent notice on our Web site of any change in ownership or uses of this information, as well as any choices you may have regarding this information. </li>
	</ul>
	
	<hr class="half-margins" /><!-- separator -->
	
	<h2>Opt-out rights</h2>
	<p>You can stop all collection of information by the Application easily by uninstalling the Application. You may use the standard uninstall processes as may be available as part of your mobile device or via the mobile application marketplace or network. You can also request to opt-out via email, at <a href="mailto:support@selfieprinter.ru">support@selfieprinter.ru</a></p>
	
	<hr class="half-margins" /><!-- separator -->

	<h2>Security</h2>
	<p>We are concerned about safeguarding the confidentiality of your information. We provide physical, electronic, and procedural safeguards to protect information we process and maintain. For example, we limit access to this information to authorized employees and contractors who need to know that information in order to operate, develop or improve our Application. Please be aware that, although we endeavor provide reasonable security for information we process and maintain, no security system can prevent all potential security breaches.</p>
	
	<hr class="half-margins" /><!-- separator -->

	<h2>Updates</h2>
	
	<p>Our Privacy Policy may change from time to time and all updates will be posted on this page <a href="http://selfieprinter.ru/privacy/en/">http://selfieprinter.ru/privacy/en/</a>. </p>
	
	<h2>Contact us</h2>
	
	<p>If you have any questions regarding privacy while using the Application, or have questions about our practices, please contact us via email at <a href="mailto:support@selfieprinter.ru">support@selfieprinter.ru</a></p>

</section>