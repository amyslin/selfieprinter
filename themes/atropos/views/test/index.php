<style>
	.overlay {
		display: none;
	}
</style>
<section class="container">
	<div class="row">
		<?php

$this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$provider,
// 		'selectableRows' => 0,
		'afterAjaxUpdate'=>'tableOnUpdate',
// 		'htmlOptions'=>array('class'=>'table-responsive','style'=>'cursor:pointer;'),
		'itemView'=>'_item',
		'ajaxUpdate' => true,
		'itemsCssClass'=>'row thumbnails',
		'template'=>'<div class="overlay"></div><div style="padding-bottom:10px;padding-top: 10px; font-size:1.1em;">{summary}</div>{items}{pager}<div style="clear: both;"/>',
		'summaryText'=>'',
		'pagerCssClass'=>'pagination',
		'pager'=>array(
			'header'=>'',
			'htmlOptions'=>array('class'=>'pagination'),
		),
));
?>
	
	</div>

</section>
<div class="modal fade" tabindex="-1" role="dialog" id="printModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Вы согласны с приведенными ниже условиями?</h4>
      </div>
      <div class="modal-body">
      	<p>Печать начнется сразу после нажатия кнопки отправить. Учтите что некоторое время потребуется для постановки документа в печать. В случае если вы не согласны с приведенными ниже условиями, закройте данный диалог</p>
      	
        <ul>
        	<li>Я подтверждаю, что выбранное мною изображение размещено мной.</li>
        	<li>Я понимаю что при нажатии на кнопку печать, мое фотография будет напечатана и не имею против этого возражений.</li>	
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary" id="printSubmit">Печать</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<?php
$url = Yii::app()->request->url;
$script = <<<SCRIPT
//$('.select2me').select2();
//$('.select2me').change(function(){ $('#frmOrgSearch').submit(); });
init();
var id = 0;
function tableOnUpdate() {
	init();
}
var interval = setInterval(function() {
		$.fn.yiiListView.update("yw0",{url:'$url'});
	}, 5000);

function init() {
	$('.print').click(function(){
		id = $(this).attr("data-href");
		console.log(id);
		return true;
	});
}
		
$('#printSubmit').click(function(){
	
		$.ajax({
			url: "http://selfieprinter.ru/test/print/id/" + id,
			dataType:"json",
		})
		.done(function(data){
			console.log(data);
			$('#printModal').modal('hide');
			$.fn.yiiListView.update("yw0",{url:'$url'});
		})
		.error(function(){
			$('#printModal').modal('hide');
			$.fn.yiiListView.update("yw0",{url:'$url'});
		});
		
});


SCRIPT;
$cs=Yii::app()->getClientScript();
$cs->registerCssFile('/css/pages/about-us.css');
// $cs->registerPackage('select2');
$cs->registerScript('init',$script, CClientScript::POS_READY);
?>

