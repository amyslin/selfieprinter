<?php
$source_data =  CJSON::decode($data->item->source_data, false);
	$username = '';
	$userlink = '#';
	$userpic = '';
	if ($data->job->type == 'instagram'&&!empty($source_data)) {
		$username = (!empty($source_data->user->username))?$source_data->user->username:$source_data->username;
		$userpic = (!empty($source_data->user->profile_picture))?$source_data->user->profile_picture:$source_data->userpic;
		$userlink = 'https://www.instagram.com/' . $username . '/';
	}
?>
<div class="col-md-4">
<div class="meet-our-team">
	<h3>#<?php echo $data->job->search_query;?><br/>
	<small><a href="<?=$userlink?>" target="_blank"><img src="<?=$userpic?>" style="border-radius: 50%; width: 40px; height: auto;"/> <?=$username?></a></small>
	</h3>

	<a href="#" class="print" data-toggle="modal" data-target="#printModal" data-href="<?php echo $data->request_id; ?>"><img src="data:image/jpeg;base64,<?php echo base64_encode(file_get_contents($data->item->source_file))?>" class="img-responsive" /></a>

	<div class="team-info text-center">
		<a href="#" data-toggle="modal" data-target="#printModal" data-href="<?php echo $data->request_id; ?>" class="btn btn-success print">Печать</a>
		<a href="#" data-href= "<?php echo $data->request_id; ?>" class="btn btn-default delete">Удалить</a>
	</div>
</div>
</div>