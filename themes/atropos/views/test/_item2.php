<?php
$source_data =  CJSON::decode($data->source_data, false);
	$username = '';
	$userlink = '#';
	$userpic = '';
	if ($data->job->type == 'instagram'&&!empty($source_data)) {
		$username = (!empty($source_data->user->username))?$source_data->user->username:$source_data->username;
		$userpic = (!empty($source_data->user->profile_picture))?$source_data->user->profile_picture:$source_data->userpic;
		$userlink = 'https://www.instagram.com/' . $username . '/';
	}
?>
<div class="col-md-3">
<div class="meet-our-team">
	<h3>#<?php echo $data->job->search_query;?><br/>
	<small><a href="<?=$userlink?>" target="_blank"><img src="<?=$userpic?>" style="border-radius: 50%; width: 40px; height: auto;"/> <?=$username?></a></small>
	</h3>

	<img src="data:image/jpeg;base64,<?php echo base64_encode(file_get_contents($data->source_file))?>" class="img-responsive" />

	<div class="team-info">
		<p></p>
		<span class="pull-right label <?php echo (empty($data->request_id)&&empty($data->print_file))?'label-default':'label-success'?>"><?php echo (empty($data->request_id)&&empty($data->print_file))?'Обработка...':'Печать'?></span>
	</div>
</div>
</div>