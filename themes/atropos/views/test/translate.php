<style>
	.overlay {
		display: none;
	}
</style>
<section class="container text-center">
	<h1>#marrakeshperm</h1>
</section>
<section class="container">

	<div class="row">
		<?php

$this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$provider,
// 		'selectableRows' => 0,
		'afterAjaxUpdate'=>'tableOnUpdate',
// 		'htmlOptions'=>array('class'=>'table-responsive','style'=>'cursor:pointer;'),
		'itemView'=>'_item2',
		'ajaxUpdate' => true,
		'itemsCssClass'=>'row thumbnails',
		'template'=>'<div class="overlay"></div><div style="padding-bottom:10px;padding-top: 10px; font-size:1.1em;">{summary}</div>{items}{pager}<div style="clear: both;"/>',
		'summaryText'=>'',
		'pagerCssClass'=>'pagination',
		'pager'=>array(
			'header'=>'',
			'htmlOptions'=>array('class'=>'pagination'),
		),
));
?>

	</div>

</section>
<?php
$url = Yii::app()->request->url;
$script = <<<SCRIPT

init();
var id = 0;
function tableOnUpdate() {
	init();
}
var interval = setInterval(function() {
		$.fn.yiiListView.update("yw0",{url:'$url'});
	}, 10000);

function init() {
	$('.print').click(function(){
		id = $(this).attr("data-href");
		console.log(id);
		return true;
	});
}


SCRIPT;
$cs=Yii::app()->getClientScript();
$cs->registerCssFile('/css/pages/about-us.css');
// $cs->registerPackage('select2');
$cs->registerScript('init',$script, CClientScript::POS_READY);
?>

