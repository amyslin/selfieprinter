<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
		<meta name="keywords" content="Инстапринтер, фотопринтер, терминал, Пермь, инстапринтер в Перми, селфипринтер, аренда инстапринтера" />
		<meta name="description" content="Аренда, установка и продажа терминалов инстапринтеров в Перми и по всей России, франшиза Инстапринтер" />
		<meta name="Author" content="LLC BountyCards" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />

		<!-- WEB FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800" rel="stylesheet" type="text/css" />
		<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<?php
			$coreScript = Yii::app()->clientScript;
			$assets = Yii::app()->assetManager->publish(Yii::app()->basePath.'/../themes/atropos', false, -1);


			$coreScript->registerPackage('jquery');
			$coreScript->registerPackage('bootstrap');
			$coreScript->registerPackage('owl-carousel');
			$coreScript->registerPackage('revolution-slider');
			$coreScript->registerPackage('modernizr');
			$coreScript->registerPackage('easing');

			$coreScript->registerCssFile($assets .'/css/font-awesome.css');
			$coreScript->registerCssFile($assets .'/css/essentials.css');
			$coreScript->registerCssFile($assets .'/css/animate.css');
			$coreScript->registerCssFile($assets .'/css/superslides.css');
			$coreScript->registerCssFile($assets . '/css/layout.css');
			$coreScript->registerCssFile($assets . '/css/layout-responsive.css');
			$coreScript->registerCssFile($assets . '/css/color_scheme/blue.css');

			$coreScript->registerScriptFile($assets.'/js/slider_revolution.js', CClientScript::POS_END);
			$coreScript->registerScriptFile($assets.'/js/scripts.js', CClientScript::POS_END);

		?>

	</head>
	<body>
		<!-- TOP NAV -->
		<header id="topNav">
			<div class="container">

				<!-- Mobile Menu Button -->
				<button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
					<i class="fa fa-bars"></i>
				</button>

				<!-- Logo text or image -->
				<a class="logo" href="/site/index">
					<?php $this->renderPartial('logo');?>
				</a>

				<!-- Top Nav -->
				<?php $this->renderPartial('menu', array('items'=>array(
					array('link'=>'#wrapper', 'title'=>'Главная'),
					array('link'=>'#about', 'title'=>'О селфипринтере'),
					array('link'=>'#how', 'title'=>'Как работает'),
					array('link'=>'#for', 'title'=>'Для чего'),
// 					array('link'=>'#about', 'title'=>'Характеристики'),
// 					array('link'=>'#about', 'title'=>'Преимущества'),
					array('link'=>'#order', 'title'=>'Заказать'),
				)));?>
				<!-- /Top Nav -->

			</div>
		</header>

		<span id="header_shadow"></span>
		<!-- /TOP NAV -->

		<!-- WRAPPER -->
		<div id="wrapper">
		<!-- REVOLUTION SLIDER -->

			<?php echo $content; ?>

		</div>

		<!-- FOOTER -->
		<footer>

			<!-- copyright , scrollTo Top -->
			<div class="footer-bar">
				<div class="container">
					<span class="copyright">&copy; ООО "Баунтикардс" - SelfiePrinter.ru</span>
					<a class="toTop" href="#topNav">На верх <i class="fa fa-arrow-circle-up"></i></a>
				</div>
			</div>
			<!-- copyright , scrollTo Top -->


			<!-- footer content -->
			<div class="footer-content">
				<div class="container">

					<div class="row">


						<!-- FOOTER CONTACT INFO -->
						<div class="column col-md-4">
							<h3>Контакты</h3>

							<p class="contact-desc">
								ООО "Баунтикардс" - реализация и аренда Инстапринтера в Перми
							</p>
							<address class="font-opensans">
								<ul>
									<li class="footer-sprite address">
										614099 Пермский край, <br />
										г.Пермь, ул. Советская 9, <br />
										гостиница "Центральная", оф. 626<br />
									</li>
									<li class="footer-sprite phone">
										Телефон: +7-999-636-59-02
									</li>
									<li class="footer-sprite email">
										<a href="mailto:artdevision@gmail.com">artdevision@gmail.com</a>
									</li>
								</ul>
							</address>

						</div>
						<!-- /FOOTER CONTACT INFO -->


						<!-- FOOTER LOGO -->
						<div class="column logo col-md-4 text-center">
							<div class="logo-content">
								<?php $this->renderPartial('logo');?>
								<h4>Инстапринтер в Перми</h4>
							</div>
						</div>
						<!-- /FOOTER LOGO -->


						<!-- FOOTER LATEST POSTS -->
						<div class="column col-md-4 text-right">
							<a href="/">Главная</a> | <a href="/privacy/ru">Политика конфедециальности</a> | <a href="/privacy/en">Privacy Policy</a>

						</div>
						<!-- /FOOTER LATEST POSTS -->

					</div>

				</div>
			</div>
			<!-- footer content -->

		</footer>
		<!-- /FOOTER -->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-72273202-1', 'auto');
		  ga('send', 'pageview');
		</script>
	</body>
</html>