<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
		<meta name="keywords" content="Инстапринтер, фотопринтер, терминал, Пермь, инстапринтер в Перми, селфипринтер, аренда инстапринтера" />
		<meta name="description" content="Аренда, установка и продажа терминалов инстапринтеров в Перми и по всей России, франшиза Инстапринтер" />
		<meta name="Author" content="LLC BountyCards" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />

		<!-- WEB FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800" rel="stylesheet" type="text/css" />
		<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<?php
			$coreScript = Yii::app()->clientScript;
			$assets = Yii::app()->assetManager->publish(Yii::app()->basePath.'/../themes/atropos', false, -1);


			$coreScript->registerPackage('jquery');
			$coreScript->registerPackage('bootstrap');
			$coreScript->registerPackage('owl-carousel');
			$coreScript->registerPackage('revolution-slider');
			$coreScript->registerPackage('modernizr');
			$coreScript->registerPackage('easing');

			$coreScript->registerCssFile($assets .'/css/font-awesome.css');
			$coreScript->registerCssFile($assets .'/css/essentials.css');
			$coreScript->registerCssFile($assets .'/css/animate.css');
			$coreScript->registerCssFile($assets .'/css/superslides.css');
			$coreScript->registerCssFile($assets . '/css/layout.css');
			$coreScript->registerCssFile($assets . '/css/layout-responsive.css');
			$coreScript->registerCssFile($assets . '/css/color_scheme/blue.css');

			$coreScript->registerScriptFile($assets.'/js/slider_revolution.js', CClientScript::POS_END);
			$coreScript->registerScriptFile($assets.'/js/scripts.js', CClientScript::POS_END);

		?>

	</head>
	<body>
		<!-- TOP NAV -->
		<header id="topNav">
			<div class="container text-center">

				<!-- Logo text or image -->
				<a class="logo" href="/site/index">
					<?php $this->renderPartial('logo');?>
				</a>

			</div>
		</header>

		<span id="header_shadow"></span>
		<!-- /TOP NAV -->

		<!-- WRAPPER -->
		<div id="wrapper">
		<!-- REVOLUTION SLIDER -->

			<?php echo $content; ?>

		</div>
	</body>
</html>